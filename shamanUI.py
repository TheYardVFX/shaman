#! /usr/bin/env python2.7
# -- coding: utf-8 --
import os
import sys
import json
import math
import random
from datetime import datetime, timedelta
import glob
import subprocess
import arronax.arronaxApi as aax
from Qt import QtGui, QtCore, QtWidgets, QtCompat


def colorFromTask(name):
    random.seed(name.split(':')[0])
    haz = random.uniform(0, 360) + 240
    color = QtGui.QColor()
    color.setHsv(haz, 80, 80)
    return color.name()


def longueur(a, b):
    """Returns the length of a 2D vector."""
    return math.sqrt(a * a + b * b)


def icon(obj, name):
    return QtGui.QIcon(os.path.join(obj.shaDirectory, 'icons', name + '.svg'))


def clearLayout(layout):
    """Recursively delete all widgets of a layout."""
    if layout is not None:
        while layout.count():
            child = layout.takeAt(0)
            try:
                child.widget()
            except:
                return
            if child.widget() is not None:
                child.widget().deleteLater()
            elif child.layout() is not None:
                clearLayout(child.layout())


def clearList(listwidget):
    """Clear a QListWidget, since the clear function is unstable."""
    while listwidget.count() > 0:
        listwidget.takeItem(0)


def set_procname(newname):
    """Change a process name"""
    from ctypes import cdll, byref, create_string_buffer
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(len(newname) + 1)
    buff.value = newname
    libc.prctl(15, byref(buff), 0, 0, 0)


class MyEdit(QtWidgets.QLineEdit):
    def __init__(self):
        super(MyEdit, self).__init__()

    def focusInEvent(self, event):
        QtCore.QTimer.singleShot(0, self.selectAll)


class BinNode(QtWidgets.QGraphicsItem):
    """Types UI objects on the left panel."""
    def __init__(self, view, version, dummy=False):
        super(BinNode, self).__init__()
        self.dummy = dummy
        self.view = view
        self.version = version
        self.over = False
        self.height = 40+20
        self.width = 80
        self.realx = 0
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.go)
        self.timerV = QtCore.QTimer()
        self.timerV.timeout.connect(self.goV)
        self.count = 0
        self.countV = 0
        self.setZValue(0)
        self.versionY = 0.0
        if not dummy:
            self.view.scene.addItem(self)
            self.setZValue(10)
            self.image = QtGui.QImage(self.version.thumbnail).scaled(self.width, self.height-20)
            self.setToolTip('%s-%s : version %s' % (version.task.asset.path, version.task.name, version.id))

    def boundingRect(self):
        return QtCore.QRectF(0, 0, self.width, self.height)

    def shape(self):
        path = QtGui.QPainterPath()
        path.addRect(QtCore.QRectF(0, 0, self.width, self.height))
        return path

    def paint(self, painter, option, widget):
        pen = QtGui.QPen()
        pen.setWidth(1.5)
        painter.setPen(pen)
        t = QtGui.QTextOption()
        t.setAlignment(QtCore.Qt.AlignHCenter)
        if not self.dummy:
            painter.drawImage(0, 0, self.image, sw=self.width, sh=self.height-20)
            font = QtGui.QFont('Whitney', 8)
            painter.setFont(font)
            painter.setBrush(QtGui.QColor(self.view.gui.selColor['selection']))
            painter.setPen(QtCore.Qt.NoPen)
            painter.drawRoundedRect(self.width-25-4, 4+self.versionY, 25, 15, 4, 4)
            pen.setColor(QtGui.QColor(255, 255, 255))
            painter.setPen(pen)
            painter.setBrush(QtGui.QColor(0, 0, 0, 0))
            painter.drawText(QtCore.QRectF(self.width-25-4, 6+self.versionY, 25, 15), str(self.version.id), t)
        pen.setColor(QtGui.QColor(self.view.gui.selColor['selection']))
        if self.over:
            pen.setColor(QtGui.QColor(self.view.gui.selColor['pieSelection']))
        painter.setPen(pen)
        painter.drawRect(0, 0, self.width, self.height)

        if not self.dummy:
            painter.setBrush(QtGui.QColor("#333333"))
            painter.drawRect(0, self.height-20, self.width, 20)
            pen.setColor(QtGui.QColor(255, 255, 255))
            painter.setPen(pen)
            path = self.version.task.asset.path.split(':', 1)[-1]
            painter.drawText(QtCore.QRectF(0, self.height-14, self.width, 20), path, t)

    def setX(self, x):
        self.realx = x
        if self.count == 0:
            self.timer.start(30)

    def go(self):
        self.setPos(self.x() * .7 + self.realx * .3, -10)
        self.count += 1
        if abs(self.x()-self.realx) < 5:
            self.count = 0
            self.timer.stop()
            self.setPos(self.realx, -10)

    def updateVersion(self):
        self.versionY = 12.0
        self.timerV.start(30)

    def goV(self):
        self.versionY = self.versionY*.8
        self.countV += 1
        if self.countV >= 20:
            self.countV = 0
            self.timerV.stop()
            self.versionY = .0
        self.update()


class PlaylistGraphView(QtWidgets.QGraphicsView):
    """Nodal graph UI."""
    def __init__(self, gui):
        super(PlaylistGraphView, self).__init__()
        self.gui = gui
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setMouseTracking(1)

        self.scene = QtWidgets.QGraphicsScene()
        self.setScene(self.scene)
        self.scene.setSceneRect(-3, 0, 80, 40)
        self.centerOn(0, 0)

        self.mousePos = (0, 0)
        self.dragStart = 0
        self.dragObj = None
        self.dummy = BinNode(self, None, dummy=True)

    def objAt(self, x, y):
        items = self.items(x, y)
        for item in reversed(items):
            if item is not self.dragObj:
                return item
        return None

    def objFullAt(self, x, y):
        items = self.items(x, y)
        for item in reversed(items):
            return item
        return None

    def mouseMoveEvent(self, event):
        obj = self.objAt(event.x(), event.y())
        for item in self.scene.items():
            item.over = False
            item.update()
        if self.dragStart == 1:
            self.dragStart = 2
            self.dummy.setPos(self.dragObj.realx, -10)
            self.dummy.version = self.dragObj.version
            self.scene.addItem(self.dummy)
        if obj:
            if self.dragStart == 0:
                obj.over = True
                obj.update()
            if self.dragStart == 2:
                self.dragObj.setPos(event.x()-self.dragObj.width*.5, event.y()-self.dragObj.height*.5)
                if obj is not self.dummy:
                    tmp = self.dummy.x()
                    self.dummy.setPos(obj.realx, -10)
                    self.dragObj.realx = obj.realx
                    obj.setX(tmp)

    def leaveEvent(self, event):
        for item in self.scene.items():
            if item is not self.dragObj:
                item.over = False
                item.update()

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            if event.modifiers() != QtCore.Qt.ControlModifier:
                self.mousePos = (event.x(), event.y())
                obj = self.objAt(event.x(), event.y())
                if obj and self.dragStart == 0:
                    self.dragStart = 1
                    self.dragObj = obj
                    self.dragObj.setZValue(100)

    def mouseReleaseEvent(self, event):
        if self.dragStart == 2:
            self.dragObj.setX(self.dragObj.realx)
            self.scene.removeItem(self.dummy)
            self.dragObj.setZValue(10)
            self.gui.playlist = [x.version for x in sorted(self.scene.items(), key=lambda y: y.realx)]
            self.dragStart = 0
            self.dragObj = None

        elif event.button() == QtCore.Qt.LeftButton:
            if event.modifiers() == QtCore.Qt.ControlModifier:
                obj = self.objFullAt(event.x(), event.y())
                if obj:
                    self.gui.removeFromPlaylist(obj)
            else:
                self.dragStart = 0
                self.dragObj = None
                obj = self.objFullAt(event.x(), event.y())
                self.gui.goto(obj.version.task)

        obj = self.objAt(event.x(), event.y())
        if obj:
            obj.over = True
            obj.update()

    def resizeEvent(self, event):
        nb = len(self.scene.items())
        if self.dragObj is not None:
            nb -= 1
        self.scene.setSceneRect(-3, 0, max(nb * 85, self.geometry().width()), 40)

    def wheelEvent(self, event):
        self.mousePos = (event.x(), event.y())
        obj = self.objAt(event.x(), event.y())
        if obj:
            index = obj.version.task.versions.index(obj.version)
            if event.delta() > 0:
                if index < len(obj.version.task.versions)-1:
                    obj.version = obj.version.task.versions[index + 1]
            else:
                if index > 0:
                    obj.version = obj.version.task.versions[index - 1]
            obj.image = QtGui.QImage(obj.version.thumbnail).scaled(obj.width, obj.height)
            obj.setToolTip('%s-%s : version %s' % (obj.version.task.asset.path, obj.version.task.name, obj.version.id))
            obj.updateVersion()


class FlowLayout(QtWidgets.QLayout):
    def __init__(self, parent=None, margin=0, spacing=-1):
        super(FlowLayout, self).__init__(parent)

        if parent is not None:
            self.setMargin(margin)

        self.setSpacing(spacing)

        self.itemList = []

    def __del__(self):
        item = self.takeAt(0)
        while item:
            item = self.takeAt(0)

    def addItem(self, item):
        self.itemList.append(item)

    def count(self):
        return len(self.itemList)

    def itemAt(self, index):
        if 0 <= index < len(self.itemList):
            return self.itemList[index]

        return None

    def takeAt(self, index):
        if 0 <= index < len(self.itemList):
            return self.itemList.pop(index)

        return None

    def expandingDirections(self):
        return QtCore.Qt.Orientations(QtCore.Qt.Orientation(0))

    def hasHeightForWidth(self):
        return True

    def heightForWidth(self, width):
        height = self._doLayout(QtCore.QRect(0, 0, width, 0), True)
        return height

    def setGeometry(self, rect):
        super(FlowLayout, self).setGeometry(rect)
        self._doLayout(rect, False)

    def sizeHint(self):
        return self.minimumSize()

    def minimumSize(self):
        size = QtCore.QSize()
        for item in self.itemList:
            if isinstance(item, QtWidgets.QListWidgetItem):
                continue
            size = size.expandedTo(item.minimumSize())
        return size

    def _doLayout(self, rect, test_only):
        x = rect.x()
        y = rect.y()
        lineHeight = 0

        for item in self.itemList:
            if isinstance(item, QtWidgets.QListWidgetItem):
                continue
            wid = item.widget()
            spaceX = self.spacing() + wid.style().layoutSpacing(
                QtWidgets.QSizePolicy.PushButton,
                QtWidgets.QSizePolicy.PushButton,
                QtCore.Qt.Horizontal)

            spaceY = self.spacing() + wid.style().layoutSpacing(
                QtWidgets.QSizePolicy.PushButton,
                QtWidgets.QSizePolicy.PushButton,
                QtCore.Qt.Vertical)

            nextX = x + item.sizeHint().width() + spaceX
            if nextX - spaceX > rect.right() and lineHeight > 0:
                x = rect.x()
                y = y + lineHeight + spaceY
                nextX = x + item.sizeHint().width() + spaceX
                lineHeight = 0

            if not test_only:
                item.setGeometry(
                    QtCore.QRect(QtCore.QPoint(x, y), item.sizeHint()))

            x = nextX
            lineHeight = max(lineHeight, item.sizeHint().height())

        return y + lineHeight - rect.y()


class MyEventFilter(QtCore.QObject):
    def __init__(self, gui,  parent=None):
        super(MyEventFilter, self).__init__(parent)
        self.gui = gui

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.MouseButtonRelease:
            self.gui.unselectAll()
        if event.type() == QtCore.QEvent.KeyPress:
            if event.key() == ord('A') and event.modifiers() == QtCore.Qt.ControlModifier:
                self.gui.selectAll()
                self.gui.setShotFilter()
        return super(MyEventFilter, self).eventFilter(obj, event)


class CommentEventFilter(QtCore.QObject):
    def __init__(self, gui,  comment, edit, color, attachHeight, hComment, cScroll, editable, widget, master):
        super(CommentEventFilter, self).__init__(parent=edit)
        self.gui = gui
        self.comment = comment
        self.edit = edit
        self.color = color
        self.attachHeight = attachHeight
        self.hComment = hComment
        self.cScroll = cScroll
        self.editable = editable
        self.widget = widget
        self.master = master
        self.destroy = False

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.MouseButtonDblClick and self.editable:
            self.gui.editComment(self.comment, self.edit, self.color, self.attachHeight, self.hComment, self.cScroll, self.master)
        if event.type() == QtCore.QEvent.MouseButtonRelease:
            if os.getenv('USER') not in self.comment.witness:
                self.comment.addWitness(os.getenv('USER'))
                self.widget.setStyleSheet('')
                self.gui.showSummary()
        if event.type() == QtCore.QEvent.Destroy:
            self.destroy = True
        if not self.destroy:
            return super(CommentEventFilter, self).eventFilter(obj, event)
        return False


class VersionEventFilter(QtCore.QObject):
    def __init__(self, gui, version, widget, comments):
        super(VersionEventFilter, self).__init__(parent=widget)
        self.gui = gui
        self.version = version
        self.widget = widget
        self.comments = comments
        self.destroy = False

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.MouseButtonRelease:
            changes = False
            if os.getenv('USER') not in self.version.witness:
                self.version.addWitness(os.getenv('USER'))
                self.widget.setStyleSheet('')
                changes = True

            for c in self.comments:
                comment, widget, edit, color = c
                if os.getenv('USER') not in comment.witness:
                    comment.addWitness(os.getenv('USER'))
                    widget.setStyleSheet('')
                    changes = True
            if changes:
                self.gui.showSummary()
        if event.type() == QtCore.QEvent.DeferredDelete:
            self.destroy = True
        if not self.destroy:
            return super(VersionEventFilter, self).eventFilter( obj, event)
        return False


class MyTaskPushButton(QtWidgets.QPushButton):
    def __init__(self, task, gui, parent=None):
        super(MyTaskPushButton, self).__init__(parent)
        self.task = task
        self.gui = gui

    def mousePressEvent(self, event):
        if event.modifiers() == QtCore.Qt.ShiftModifier:
            self.gui.shiftSelectTask(self.task)
        elif event.modifiers() == QtCore.Qt.ControlModifier:
            self.gui.controlSelectTask(self.task)
        else:
            self.gui.singleSelectTask(self.task)
        event.accept()

    def mouseReleaseEvent(self, event):
        event.accept()


class MyAssetPushButton(QtWidgets.QPushButton):
    def __init__(self, asset, gui, parent=None):
        super(MyAssetPushButton, self).__init__(parent)
        self.asset = asset
        self.gui = gui

    def mousePressEvent(self, event):
        if event.modifiers() == QtCore.Qt.ShiftModifier:
            self.gui.shiftSelectAsset(self.asset)
        elif event.modifiers() == QtCore.Qt.ControlModifier:
            self.gui.controlSelectAsset(self.asset)
        else:
            self.gui.singleSelectAsset(self.asset)
        event.accept()

    def mouseReleaseEvent(self, event):
        event.accept()


class MyWindow(QtWidgets.QMainWindow):
    def __init__(self, gui):
        super(MyWindow, self).__init__()
        self.gui = gui

    def closeEvent(self, event):
        with open(self.gui.settingsFile) as fid:
            root = json.load(fid)
        root['window_width'] = self.geometry().width()
        root['window_height'] = self.geometry().height()
        root['window_x'] = self.geometry().x()
        root['window_y'] = self.geometry().y()
        root['splitter'] = self.gui.root.splitter.sizes()
        root['summary'] = self.gui.root.summaryWidget.isVisible()
        lines = json.dumps(root, sort_keys=True, indent=4)
        with open(self.gui.settingsFile, 'w') as fid:
            fid.writelines(lines)

        QtWidgets.QMainWindow.closeEvent(self, event)


class SummaryButton(QtWidgets.QPushButton):
    def __init__(self, gui, text):
        super(SummaryButton, self).__init__(text)
        self.labv = None
        self.labc = None
        self.gui = gui
        self.setMouseTracking(True)
        self.color = ''

    def setColor(self, color=None):
        if color:
            self.color = color
        self.setStyleSheet('''background: %s; text-align:left''' % self.color)

    def setWidgets(self, labv, labc):
        self.labv = labv
        self.labc = labc

    def mouseMoveEvent(self, event):
        super(SummaryButton, self).mouseMoveEvent(event)
        for x in [self, self.labv, self.labc]:
            x.setStyleSheet('''background: %s; text-align:left''' % self.gui.selColor['selection'])

    def leaveEvent(self, event):
        super(SummaryButton, self).leaveEvent(event)
        for x in [self, self.labv, self.labc]:
            x.setStyleSheet('''background: %s; text-align:left''' % self.color)


class ShamanGUI(object):
    """Main UI."""
    def __init__(self, app):
        self.shaDirectory = os.path.dirname(os.path.abspath(__file__)).replace('\\', '/')
        self.app = app
        if not os.path.exists(os.path.join(os.getenv('HOME'), 'shaman')):
            os.makedirs(os.path.join(os.getenv('HOME'), 'shaman'))
        self.settingsFile = os.path.join(os.getenv('HOME'), 'shaman', 'settings.info')
        if not os.path.exists(self.settingsFile):
            lines = json.dumps({'filters': {}, 'width': 100}, sort_keys=True, indent=4)
            with open(self.settingsFile, 'w') as fid:
                fid.writelines(lines)
        for f in glob.glob(self.shaDirectory + '/fonts/*.ttf'):
            QtGui.QFontDatabase.addApplicationFont(f)
        self.window = MyWindow(self)
        mainUIPath = os.path.join(self.shaDirectory, 'UI', 'main.ui')
        self.root = QtCompat.loadUi(mainUIPath)
        self.window.setCentralWidget(self.root)
        iconPath = os.path.join(self.shaDirectory, 'icons', 'shaIcon.svg')
        self.window.setWindowIcon(QtGui.QIcon(iconPath))
        playlistPlayButton = PlayButton(self)
        self.root.playlistWidget.layout().insertWidget(0, playlistPlayButton)

        playlistPlayButton.setIcon(icon(self, 'play-white-circle'))
        playlistPlayButton.setIconSize(QtCore.QSize(32, 32))
        playlistPlayButton.setMinimumWidth(60)
        playlistPlayButton.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)

        self.root.playlistClearButton.setIcon(icon(self, 'eraser'))
        self.root.savePlaylistPushButton.setIcon(icon(self, 'save-white'))
        self.root.addToPlaylistPushButton.setIcon(icon(self, 'play-white-plus'))
        self.root.deletePlaylistPushButton.setIcon(icon(self, 'trash-white'))
        self.root.refreshProjectButton.setIcon(icon(self, 'refresh-cw'))
        self.root.warningButton.setIcon(icon(self, 'refresh-cw'))
        self.root.mangroveButton.setIcon(QtGui.QIcon(os.path.join(self.shaDirectory, '../mgv/icons', 'mgvIcon.svg')))
        self.root.userPushButton.setIcon(icon(self, 'user'))
        self.root.propertiesButton.setIcon(icon(self, 'info'))
        self.root.readTaskButton.setIcon(icon(self, 'read'))
        self.root.addToPlaylistPushButton.setIconSize(QtCore.QSize(16, 16))
        self.root.refreshProjectButton.setIconSize(QtCore.QSize(16, 16))
        self.root.warningButton.setIconSize(QtCore.QSize(24, 24))
        self.root.mangroveButton.setIconSize(QtCore.QSize(24, 24))
        self.root.userPushButton.setIconSize(QtCore.QSize(16, 16))
        self.root.showPlaylistButton.setIconSize(QtCore.QSize(16, 16))
        self.root.readTaskButton.setIconSize(QtCore.QSize(24, 24))
        self.root.readTaskButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.mangroveButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.warningButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.propertiesButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.pathLabel.setStyleSheet("font: 18pt 'Whitney Medium'; ")
        self.root.shotTaskComboBox.setStyleSheet("font: 16pt 'Whitney Light';")
        self.root.multiPreferredTaskComboBox.setStyleSheet("font: 16pt 'Whitney Light';")
        self.root.summaryButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.showPlaylistButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.addToPlaylistPushButton.setFixedHeight(34)

        self.root.playlistClearButton.setIconSize(QtCore.QSize(20, 20))
        self.root.savePlaylistPushButton.setIconSize(QtCore.QSize(24, 24))
        self.root.deletePlaylistPushButton.setIconSize(QtCore.QSize(24, 24))
        self.root.playlistClearButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.savePlaylistPushButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        self.root.deletePlaylistPushButton.setStyleSheet('QPushButton::!hover {background: transparent}')

        colorPath = os.path.join(self.shaDirectory, 'UI', 'color.json')
        with open(colorPath) as fid:
            root = json.load(fid)
        self.colorDic = root['level']
        self.selColor = root['UI']
        self.style = open(self.shaDirectory + '/style.css').read().replace('url(:', 'url(' + self.shaDirectory)
        for item in root['UI']:
            self.style = self.style.replace('$%s' % item, root['UI'][item])
        self.root.setStyleSheet(self.style)

        r = self.window.geometry()
        r.moveCenter(QtWidgets.QApplication.desktop().availableGeometry().center())
        self.window.setGeometry(r)
        with open(self.settingsFile) as fid:
            root = json.load(fid)
        if 'window_width' in root:
            self.window.setGeometry(root['window_x'], root['window_y'], root['window_width'], root['window_height'])
        if 'splitter' in root:
            self.root.splitter.setSizes(root['splitter'])

        self.window.show()
        self.window.setWindowTitle('Shaman 3.0')
        self.root.widgetMultiTask.hide()
        self.root.widgetOneTask.hide()
        self.root.widgetShot.hide()
        self.togglePlayList()

        cornerWidget = QtWidgets.QWidget()
        hh = QtWidgets.QHBoxLayout()
        hh.setContentsMargins(0, 0, 0, 0)
        cornerWidget.setLayout(hh)

        self.readButton = QtWidgets.QPushButton()
        self.readButton.setIcon(icon(self, 'read'))
        self.readButton.setStyleSheet('QPushButton::!hover{background: transparent;}')
        self.readButton.setFixedHeight(40)

        timelogButton = QtWidgets.QPushButton()
        timelogButton.setIcon(icon(self, 'schedule'))
        timelogButton.setStyleSheet('QPushButton::!hover{background: transparent}')
        timelogButton.setFixedHeight(40)
        timelogButton.clicked.connect(self.timeLog)

        hh.addWidget(timelogButton)
        hh.addWidget(self.readButton)
        self.root.summaryTab.layout().addWidget(cornerWidget)
        self.root.sumTabWidget.setCornerWidget(cornerWidget)

        self.flow = FlowLayout()
        self.root.desk.setLayout(self.flow)

        self.currentProject = None
        self.assets = []
        self.tasks = []
        self.assignedUsers = {}
        self.assignedTags = {}
        self.filterTokens = []
        self.userTokens = []
        self.assetTagTokens = []
        self.selectedTasks = []
        self.selectedAssets = []
        self.taskDico = {}
        self.playlist = []
        self.auto = False
        self.taskWidth = 100
        self.WTimer = None
        self.userWindow = None
        self.commentWindow = None
        self.timeLogWindow = None
        self.zoomWidgets = {}
        self.zooming = False
        self.preferredTask = None
        self.statGraph = None
        self.statScene = None
        self.statCount = None
        self.commentEvents = []
        self.versionEvents = []
        self.toMark = []
        self.toggleSummary()

        with open(self.settingsFile) as fid:
            root = json.load(fid)

        if 'width' in root.keys():
            self.taskWidth = root['width']
            self.root.slider.setValue(self.taskWidth)
        if 'shot' in root.keys():
            self.root.shotCheckBox.setChecked(root['shot'])

        self.root.projectComboBox.addItems(aax.getProjectNames())

        self.root.projectComboBox.currentIndexChanged.connect(lambda i: self.changeProject())
        self.root.patternComboBox.currentIndexChanged.connect(self.changePattern)
        self.root.keyComboBox.currentIndexChanged.connect(self.changeKey)
        self.root.assetsListWidget.itemSelectionChanged.connect(self.setShotFilter)
        self.root.assignedLineEdit.returnPressed.connect(self.completerAssignActivated)
        self.root.assetTagLineEdit.returnPressed.connect(self.completerAssetTagActivated)

        self.root.playlistClearButton.clicked.connect(self.clearPlaylist)
        self.root.showPlaylistButton.clicked.connect(self.togglePlayList)
        self.root.shotCheckBox.stateChanged.connect(self.changeAssetTask)
        self.root.taskTableWidget.itemSelectionChanged.connect(self.selectTaskRows)
        self.root.addToPlaylistPushButton.clicked.connect(self.addAllToPlaylist)
        self.root.playlistComboBox.currentIndexChanged.connect(self.loadPlaylist)
        self.root.deletePlaylistPushButton.clicked.connect(self.deletePlaylist)
        self.root.savePlaylistPushButton.clicked.connect(self.savePlaylist)
        self.root.refreshProjectButton.clicked.connect(self.changeProject)
        self.root.warningButton.clicked.connect(self.changeTask)
        self.root.mangroveButton.clicked.connect(self.goToMgv)
        self.root.userPushButton.clicked.connect(self.userUI)
        self.root.sortComboBox.currentIndexChanged.connect(self.showDesk)
        self.root.slider.sliderMoved.connect(self.zoom)
        self.root.slider.sliderPressed.connect(self.zoomStarted)
        self.root.slider.sliderReleased.connect(self.zoomFinished)
        self.root.slider.valueChanged.connect(self.zoomChanged)
        self.root.summaryButton.clicked.connect(self.toggleSummary)
        self.root.shotTaskComboBox.currentIndexChanged.connect(self.shotTaskChanged)
        self.root.multiPreferredTaskComboBox.currentIndexChanged.connect(self.shotPreferredTaskChanged)
        self.root.propertiesButton.clicked.connect(self.showProperties)
        self.root.sumTabWidget.currentChanged.connect(self.showStats)
        self.readButton.clicked.connect(self.markAllRead)
        self.root.readTaskButton.clicked.connect(self.markTaskRead)
        self.root.savePlaylistPushButton.setEnabled(False)
        self.root.deletePlaylistPushButton.setEnabled(False)
        self.root.savePlaylistPushButton.setIcon(icon(self, 'save-grey'))
        self.root.deletePlaylistPushButton.setIcon(icon(self, 'trash-grey'))

        self.f1 = FilterObject(self, self.root.widgetFilters.layout(), "task")
        self.f2 = FilterObject(self, self.root.summaryTab.layout(), "summary")

        self.myevent = MyEventFilter(self)
        self.root.desk.installEventFilter(self.myevent)

        self.view = PlaylistGraphView(self)
        self.view.setFixedHeight(40+22)
        self.root.playlistLayout.addWidget(self.view)

        # installe le QCompleter Assign #############################################################
        completer = QtWidgets.QCompleter(self.root.assignedLineEdit)
        completer.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        self.root.assignedLineEdit.setCompleter(completer)
        # installe le model à partir de la liste fournie datas
        self.amodel = QtGui.QStandardItemModel()
        # installe le QSortFilterProxyModel qui s'insère entre le QCompleter et le model
        proxymodel = QtCore.QSortFilterProxyModel(self.root.assignedLineEdit)
        proxymodel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        proxymodel.setSourceModel(self.amodel)
        completer.setModel(proxymodel)
        # chaque changement de texte déclenchera la filtration du model
        self.root.assignedLineEdit.textEdited.connect(proxymodel.setFilterFixedString)
        completer.activated.connect(self.completerAssignActivated, QtCore.Qt.DirectConnection)

        # installe le QCompleter Asset Tag ##########################################################
        completer = QtWidgets.QCompleter(self.root.assetTagLineEdit)
        completer.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        self.root.assetTagLineEdit.setCompleter(completer)
        # installe le model à partir de la liste fournie datas
        self.smodel = QtGui.QStandardItemModel()
        # installe le QSortFilterProxyModel qui s'insère entre le QCompleter et le model
        proxymodel = QtCore.QSortFilterProxyModel(self.root.assetTagLineEdit)
        proxymodel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        proxymodel.setSourceModel(self.smodel)
        completer.setModel(proxymodel)
        # chaque changement de texte déclenchera la filtration du model
        self.root.assetTagLineEdit.textEdited.connect(proxymodel.setFilterFixedString)
        completer.activated.connect(self.completerAssetTagActivated, QtCore.Qt.DirectConnection)

        pref = os.path.join(os.getenv('HOME'), 'mangrove1.0', 'project.info')
        try:
            with open(pref) as fid:
                lines = fid.readlines()
            index = self.root.projectComboBox.findText(lines[0].strip(), flags=QtCore.Qt.MatchFixedString)
            if index > -1:
                self.root.projectComboBox.setCurrentIndex(index)
            else:
                self.root.projectComboBox.setCurrentIndex(0)
        except (OSError, IOError):
            self.root.projectComboBox.setCurrentIndex(0)
            self.changeProject(auto=True)

        if 'summary' in root:
            if root['summary'] is True:
                self.toggleSummary()

    def goToMgv(self):
        import socket
        from mgv import mgvCom
        task = self.selectedTasks[0]
        path = self.currentProject.codeName + ':' + task.asset.path
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect(('8.8.8.8', 0))
            os.environ['MGVIP'] = s.getsockname()[0]
        except:
            os.environ['MGVIP'] = '127.0.0.1'
        for port in range(10000, 10010):
            os.environ['MGVPORT'] = str(port)
            mgvCom.open(path)

    def toggleSummary(self):
        if self.root.summaryWidget.isVisible():
            self.root.summaryWidget.hide()
            self.root.summaryButton.setText('<')
        else:
            self.root.summaryWidget.show()
            self.root.summaryButton.setText('>')
            self.showSummary()

    def showProperties(self):
        d = QtWidgets.QDialog(self.root)
        d.setWindowTitle('Properties')
        v = QtWidgets.QVBoxLayout()

        for x in self.selectedTasks[0].asset.dico:
            h = QtWidgets.QHBoxLayout()
            lab = QtWidgets.QLabel(x + ':')
            lab.setFixedWidth(150)
            lab.setStyleSheet('font: medium')
            val = QtWidgets.QLabel(self.selectedTasks[0].asset.dico[x])
            val.setWordWrap(True)
            h.addWidget(lab)
            h.addWidget(val)
            v.addLayout(h)
        v.addStretch()
        d.setLayout(v)
        d.show()
        d.resize(800, 400)

        r = d.geometry()
        r.moveCenter(QtWidgets.QApplication.desktop().availableGeometry().center())
        d.setGeometry(r)

    def zoomStarted(self):
        self.zooming = True

    def zoomFinished(self):
        self.zooming = False
        with open(self.settingsFile) as fid:
            root = json.load(fid)
        root['width'] = self.taskWidth
        lines = json.dumps(root, sort_keys=True, indent=4)
        with open(self.settingsFile, 'w') as fid:
            fid.writelines(lines)

    def zoomChanged(self):
        self.zoom()
        if self.zooming is False:
            self.zoomFinished()

    def userUI(self):
        self.userWindow = UserUI(self)

    def showPlayListWidget(self):
        if not self.root.playlistWidget.isVisible():
            self.togglePlayList()

    def togglePlayList(self):
        if self.root.playlistWidget.isVisible():
            self.root.playlistWidget.hide()
            self.root.showPlaylistButton.setIcon(icon(self, 'chevron-up'))
        else:
            self.root.playlistWidget.show()
            self.root.showPlaylistButton.setIcon(icon(self, 'chevron-down'))

    def loadPlaylist(self):
        text = self.root.playlistComboBox.currentText()
        if len(text):
            self.playlist = []
            playlist = self.currentProject.getPlaylist(text)
            if playlist:
                self.playlist = list(playlist.versions)
                self.setPlaylistButtons()
            self.showPlaylist()

    def deletePlaylist(self):
        text = self.root.playlistComboBox.currentText()
        if len(text):
            result = QtWidgets.QMessageBox.question(self.root, 'Confirm',
                                                    'Are you sure to delete playlist %s ?' % text,
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)
            if result == QtWidgets.QMessageBox.Yes:
                playlist = self.currentProject.getPlaylist(text)
                if playlist:
                    playlist.delete()
                    self.setPlaylistButtons()
                    self.root.playlistComboBox.clear()
                    self.root.playlistComboBox.addItems([''] + [x.name for x in self.currentProject.playlists])
                    self.root.playlistComboBox.setCurrentIndex(0)

    def savePlaylist(self):
        if len(self.playlist):
            text = self.root.playlistComboBox.currentText()
            name, ok = QtWidgets.QInputDialog.getText(self.root, 'Save Playlist', 'Name :', text=text)
            if ok:
                playlist = self.currentProject.getPlaylist(name)
                if playlist:
                    result = QtWidgets.QMessageBox.question(self.root, 'Confirm',
                                                            'Do you want to replace existing playlist %s ?' % text,
                                                            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                            QtWidgets.QMessageBox.No)
                    if result == QtWidgets.QMessageBox.Yes:
                        playlist.delete()
                aax.Playlist(project=self.currentProject, name=name, versions=list(self.playlist)).create()
                self.root.playlistComboBox.clear()
                liste = [''] + [x.name for x in self.currentProject.playlists]
                self.root.playlistComboBox.addItems(liste)
                index = liste.index(name)
                self.root.playlistComboBox.setCurrentIndex(index)

    def selectTaskRows(self):
        self.root.addToPlaylistPushButton.setFocus(QtCore.Qt.OtherFocusReason)

    def changeAssetTask(self):
        self.selectedAssets = []
        self.selectedTasks = []
        self.root.widgetMultiTask.hide()
        self.root.widgetOneTask.hide()
        self.root.widgetShot.hide()
        self.filterDesk()

        with open(self.settingsFile) as fid:
            root = json.load(fid)
        root['shot'] = self.root.shotCheckBox.isChecked()
        lines = json.dumps(root, sort_keys=True, indent=4)
        with open(self.settingsFile, 'w') as fid:
            fid.writelines(lines)

    def unselectAll(self):
        self.selectedAssets = []
        self.selectedTasks = []
        self.root.widgetMultiTask.hide()
        self.root.widgetOneTask.hide()
        self.root.widgetShot.hide()
        self.setShotFilter()

    def selectAll(self):
        self.selectedAssets = []
        self.selectedTasks = []
        shot = self.root.shotCheckBox.isChecked()
        if shot:
            assets = list(set([x.asset for x in self.tasks]))
            for asset in assets:
                if asset not in self.selectedAssets:
                    widgets = self.taskDico[asset.uuid]
                    for widget in widgets[:-1]:
                        widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
                    self.selectedAssets.append(asset)
            self.changeAsset()
        else:
            for task in self.tasks:
                if task not in self.selectedTasks:
                    widgets = self.taskDico[task.uuid]
                    for widget in widgets[:-1]:
                        widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
                    self.selectedTasks.append(task)
            self.changeTask()

    def completerAssignActivated(self):
        s = self.root.assignedLineEdit.text()
        if len(s):
            self.addAssigned(s)
        QtCore.QTimer.singleShot(0, self.root.assignedLineEdit.clear)

    def completerAssetTagActivated(self):
        s = self.root.assetTagLineEdit.text()
        if len(s):
            self.addAssetTag(s)
        QtCore.QTimer.singleShot(0, self.root.assetTagLineEdit.clear)

    def setFilterTokens(self):
        self.filterTokens = set()
        self.userTokens = set()
        self.assetTagTokens = set()
        for asset in self.currentProject.assets:
            for tag in asset.tags:
                self.filterTokens.add('tag:' + tag)
                self.assetTagTokens.add(tag)
            for task in asset.tasks:
                self.filterTokens.add('task:'+task.name)
                for version in task.versions:
                    self.filterTokens.add('status:' + version.status)
                    self.filterTokens.add('user:' + version.user)
                    self.userTokens.add(version.user)
        for user in self.currentProject.users:
            self.filterTokens.add('user:' + user.name)
            self.userTokens.add(user.name)
        self.f1.model.clear()
        self.f2.model.clear()
        self.amodel.clear()
        self.smodel.clear()

        for i, word in enumerate(self.filterTokens):
            item = QtGui.QStandardItem(word)
            self.f1.model.setItem(i, 0, item)
            item = QtGui.QStandardItem(word)
            self.f2.model.setItem(i, 0, item)
        for i, word in enumerate(self.userTokens):
            item = QtGui.QStandardItem(word)
            self.amodel.setItem(i, 0, item)
        for i, word in enumerate(self.assetTagTokens):
            item = QtGui.QStandardItem(word)
            self.smodel.setItem(i, 0, item)

    def addAssigned(self, text, update=True):
        user = self.currentProject.getUser(text)
        if user is None:
            user = self.currentProject.addUser(text)
        if text not in self.assignedUsers:
            new = UserButton(self, user)
            self.assignedUsers[text] = new
            self.root.assignedLayout.addWidget(new)
            item = QtGui.QStandardItem(text)
            self.amodel.setItem(self.smodel.rowCount(), 0, item)

        if update:
            path = ':'.join(['path', self.selectedTasks[0].asset.path, self.selectedTasks[0].name])
            user.addAssign(path)
            self.root.assignedLineEdit.clear()
        self.setFilterTokens()

    def removeAssigned(self, user, assignment):
        user.removeAssign(assignment)
        self.root.assignedLayout.removeWidget(self.assignedUsers[user.name])
        self.assignedUsers[user.name].deleteLater()
        del self.assignedUsers[user.name]
        self.setFilterTokens()

    def addAssetTag(self, text):
        if text not in self.assignedTags:
            self.assignedTags[text] = TagButton(self, text)
            self.root.assetTagLayout.addWidget(self.assignedTags[text])
        self.selectedAssets[0].addTag(text)
        self.root.assetTagLineEdit.clear()
        self.setFilterTokens()

    def removeAssetTag(self, text):
        self.root.assetTagLayout.removeWidget(self.assignedTags[text])
        self.assignedTags[text].deleteLater()
        del self.assignedTags[text]
        self.selectedAssets[0].removeTag(text)
        self.setFilterTokens()

    def changeProject(self, auto=False):
        self.currentProject = aax.Project.Project(self.root.projectComboBox.currentText())
        self.taskDico = {}
        self.selectedTasks = []
        self.auto = True
        self.root.patternComboBox.clear()
        self.root.keyComboBox.clear()
        assets = [x.path for x in self.currentProject.assets if x.omit is False]
        keys = list(set([x.split(':')[1] for x in assets]))
        patterns = {}
        for x in assets:
            name = x.split(':')[0]
            if name not in patterns:
                patterns[name] = 0
            patterns[name] += 1
        self.root.patternComboBox.addItems(sorted(patterns.keys(), key=lambda x: -patterns[x]))
        self.root.keyComboBox.addItems([''] + sorted(keys))
        self.root.playlistComboBox.clear()
        self.root.playlistComboBox.addItems([''] + [x.name for x in self.currentProject.playlists])
        self.root.playlistComboBox.setCurrentIndex(0)
        self.auto = False
        self.changePattern()

        with open(self.settingsFile) as fid:
            root = json.load(fid)

        self.auto = True
        self.f1.setItems(['', 'me'] + root['filters'].keys() + self.currentProject.availableTasks)
        self.f2.setItems(['', 'me'] + root['filters'].keys() + self.currentProject.availableTasks)
        self.auto = False
        self.f1.root.filtersComboBox.setCurrentIndex(1)
        self.f2.root.filtersComboBox.setCurrentIndex(1)

        if not auto:
            self.changeTask()
            pref = os.path.join(os.getenv('HOME'), 'mangrove1.0', 'project.info')
            try:
                with open(pref, 'w') as fid:
                    fid.write(self.root.projectComboBox.currentText())
            except (OSError, IOError):
                pass

    def changePattern(self):
        if not self.auto:
            self.filterList()
            self.setFilterTokens()

    def changeKey(self):
        if not self.auto:
            self.filterList()
            self.setFilterTokens()
            self.auto = True
            self.smartSelect()
            self.auto = False
            self.setShotFilter()

    def smartSelect(self):
        if len(self.assets) == 0:
            self.root.assetsListWidget.selectAll()

    def filterList(self):
        pat = self.root.patternComboBox.currentText()
        key = self.root.keyComboBox.currentText()
        assets = [x for x in self.currentProject.assets if x.omit is False and x.path.split(':')[0] == pat]
        if len(key):
            assets = [x for x in assets if x.path.split(':')[1] == key]

        asset_ok = self.combineFilters(assets, self.f1.filters)
        assets = [':'.join(each_asset.path.split(':')[1:]) for each_asset in asset_ok]
        assets.sort()

        self.auto = True
        clearList(self.root.assetsListWidget)
        self.root.assetsListWidget.addItems(assets)

        for i in range(self.root.assetsListWidget.count()):
            item = self.root.assetsListWidget.item(i)
            text = '%s:%s' % (pat, item.text())
            if text in self.assets:
                self.root.assetsListWidget.item(i).setSelected(True)

        self.auto = False

    def combineFilters(self, assets, dico, mode='asset'):
        asset_ok = []
        task_ok = []
        for asset in assets:
            if asset.omit:
                continue
            if len(asset.tasks) == 0 and mode == 'asset':
                ok = True
                for f in dico.keys():
                    tagType, tag = f.split(':', 1)
                    if tagType == 'tag':
                        if tag not in asset.tags and tag not in asset.path:
                            ok = False
                            break
                    if tagType in ['user', 'status', 'task']:
                        ok = False
                        break
                if ok:
                    asset_ok.append(asset)
                    continue

            for task in asset.tasks:
                ok = True
                for f in dico.keys():
                    tagType, tag = f.split(':', 1)
                    if tagType == 'tag':
                        if dico[f].inverted:
                            if tag in asset.tags or tag in asset.path:
                                ok = False
                                break
                        else:
                            if tag not in asset.tags and tag not in asset.path:
                                ok = False
                                break
                    if tagType == 'user':
                        if dico[f].inverted:
                            if tag not in [ver.user for ver in task.versions]:
                                continue
                        else:
                            if tag in [ver.user for ver in task.versions]:
                                continue
                        user = self.currentProject.getUser(tag)
                        if user is None:
                            ok = False
                            break
                        m = user.match(':'.join(['path', task.asset.path, task.name]))
                        if dico[f].inverted:
                            if m is not False:
                                ok = False
                                break
                            for tag in asset.tags:
                                if user.match('tag:' + tag):
                                    ok = False
                                    break
                        else:
                            if m is False:
                                found = False
                                for tag in asset.tags:
                                    if user.match('tag:' + tag):
                                        found = True
                                        break
                                if found is False:
                                    ok = False
                                    break
                    if tagType == 'status':
                        if dico[f].inverted:
                            if len(task.versions):
                                if tag == task.versions[-1].status:
                                    ok = False
                                    break
                            else:
                                if tag == 'Not Started':
                                    ok = False
                                    break
                        else:
                            if not len(task.versions) and tag != 'Not Started':
                                ok = False
                                break
                            elif tag != task.versions[-1].status:
                                ok = False
                                break
                    if tagType == 'task':
                        if dico[f].inverted:
                            if tag in task.name:
                                ok = False
                                break
                        else:
                            if tag not in task.name:
                                ok = False
                                break
                if ok:
                    if mode == 'task':
                        task_ok.append(task)
                    else:
                        asset_ok.append(asset)
                        break
        if mode == 'task':
            return task_ok
        return asset_ok

    def setShotFilter(self, refresh=True):
        if not self.auto:
            self.assets = [self.root.patternComboBox.currentText() + ':' + item.text()
                           for item in self.root.assetsListWidget.selectedItems()]
            if refresh:
                self.filterDesk()

    def filterDesk(self):
        self.tasks = self.combineFilters([x for x in self.currentProject.assets if x.path in self.assets], self.f1.filters, 'task')
        self.showDesk()

    def zoom(self):
        self.taskWidth = self.root.slider.value()
        for w in self.zoomWidgets:
            if self.zoomWidgets[w] is not None:
                w.setIconSize(QtCore.QSize(self.taskWidth, self.taskWidth * 9 / 16))
                w.setFixedSize(self.taskWidth, self.taskWidth * 9 / 16)
            else:
                w.setMaximumSize(self.taskWidth, self.taskWidth / 5)

    def showDesk(self):
        self.taskDico = {}
        self.zoomWidgets = {}
        clearLayout(self.flow)
        self.app.processEvents()
        shot = self.root.shotCheckBox.isChecked()
        if shot:
            tasks = set([x.asset for x in self.tasks])
            self.root.multiPreferredTaskComboBox.show()
        else:
            self.root.multiPreferredTaskComboBox.hide()
            tasks = self.tasks
        if self.root.sortComboBox.currentText() == "Name":
            if shot:
                tasks = sorted(tasks, key=lambda x: x.path)
            else:
                tasks = sorted(tasks, key=lambda x: x.asset.path+x.name)
        if self.root.sortComboBox.currentText() == "Last update":
            if shot:
                tasks = sorted(tasks, key=lambda x: max([y.updated for y in x.tasks] if len(x.tasks) else -1))
            else:
                tasks = sorted(tasks, key=lambda x: x.updated)
        for i, task in enumerate(tasks):
            mainWidget = QtWidgets.QWidget()
            self.flow.addWidget(mainWidget)
            box = QtWidgets.QVBoxLayout()
            mainWidget.setLayout(box)
            box.setContentsMargins(0, 0, 0, 0)
            box.setSpacing(0)

            if shot:
                vignette = MyAssetPushButton(task, self)
                n1 = MyAssetPushButton(task, self)
                n1.setText(':'.join(task.path.split(':')[1:]))
                versions = sum([x.versions for x in task.tasks], [])
                if len(versions):
                    versions = sorted(versions, key=lambda x: x.date)
                    vignette.setIcon(QtGui.QIcon(versions[-1].thumbnail))
                    vignette.setIconSize(QtCore.QSize(self.taskWidth, self.taskWidth * 9 / 16))
                    self.zoomWidgets[vignette] = versions[-1].thumbnail
                else:
                    self.zoomWidgets[vignette] = ''
                vignette.clicked.connect(lambda a=task: self.singleSelectAsset(a))
            else:
                vignette = MyTaskPushButton(task, self)
                n1 = MyTaskPushButton(task, self)
                if len(task.versions):
                    vignette.setIcon(QtGui.QIcon(task.versions[-1].thumbnail))
                    vignette.setIconSize(QtCore.QSize(self.taskWidth, self.taskWidth * 9 / 16))
                    self.zoomWidgets[vignette] = task.versions[-1].thumbnail
                else:
                    self.zoomWidgets[vignette] = ''
                vignette.clicked.connect(lambda t=task: self.singleSelectTask(t))
                n1.setText(':'.join(task.asset.path.split(':')[1:]))
            n1.setMaximumSize(self.taskWidth, self.taskWidth / 5)
            self.zoomWidgets[n1] = None
            box.addWidget(n1)
            vignette.setFlat(True)
            vignette.setFixedSize(self.taskWidth, self.taskWidth * 9 / 16)
            box.addWidget(vignette)
            box.setAlignment(vignette, QtCore.Qt.AlignHCenter)
            if shot:
                color = self.selColor['edit']
                self.taskDico[task.uuid] = [vignette, n1, color]
                if task in self.selectedAssets:
                    vignette.setStyleSheet('background: %s; border-radius: 0px' % self.selColor['selection'])
                    n1.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
                else:
                    vignette.setStyleSheet('background: %s; border-radius: 0px' % color)
                    n1.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % color)
            else:
                color = colorFromTask(task.name)
                n2 = MyTaskPushButton(task, self)
                n2.setText(task.name)
                box.addWidget(n2)
                n2.setMaximumSize(self.taskWidth, self.taskWidth / 5)
                self.zoomWidgets[n2] = None
                color2 = color
                if task in self.selectedTasks:
                    vignette.setStyleSheet('background: %s; border-radius: 0px' % self.selColor['selection'])
                    n1.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
                    n2.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
                else:
                    vignette.setStyleSheet('background: %s; border-radius: 0px' % color)
                    n1.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % color)
                    if not len(task.versions):
                        n2.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % color)
                    else:
                        color2 = self.currentProject.states[task.versions[-1].status]
                        n2.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % color2)
                self.taskDico[task.uuid] = [vignette, n1, n2, color, color2]
        change = False
        if shot:
            for sel in list(self.selectedAssets):
                if sel not in tasks:
                    self.selectedAssets.remove(sel)
                    change = True
            if change:
                self.changeAsset()
        else:
            for sel in self.selectedTasks:
                if sel not in tasks:
                    self.selectedTasks.remove(sel)
                    change = True
            if change:
                self.changeTask()

    def shiftSelectAsset(self, asset):
        if not len(self.selectedAssets):
            self.controlSelectAsset(asset)
            return
        assets = set([x.asset for x in self.tasks])

        if self.root.sortComboBox.currentText() == "Name":
            assets = sorted(assets, key=lambda x: x.path)
        if self.root.sortComboBox.currentText() == "Last update":
            assets = sorted(assets, key=lambda x: max([y.updated for y in x.assets] if len(x.assets) else -1))
        i1 = assets.index(self.selectedAssets[-1])
        i2 = assets.index(asset)
        if i2 < i1:
            i1, i2 = i2, i1
        liste = assets[i1:i2 + 1]
        for asset in liste:
            if asset not in self.selectedAssets:
                widgets = self.taskDico[asset.uuid]
                for widget in widgets[:3]:
                    widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
                self.selectedAssets.append(asset)
        self.changeAsset()

    def controlSelectAsset(self, asset):
        if asset not in self.selectedAssets:
            self.selectedAssets.append(asset)
            widgets = self.taskDico[asset.uuid]
            for widget in widgets[:3]:
                widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
            self.changeAsset()

    def singleSelectAsset(self, asset):
        for st in self.selectedAssets:
            widgets = self.taskDico[st.uuid]
            for widget in widgets[:3]:
                widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % widgets[3])
            widgets[2].setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % widgets[4])
        self.selectedAssets = [asset]
        widgets = self.taskDico[asset.uuid]
        for widget in widgets[:3]:
            widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
        self.changeAsset()

    def shiftSelectTask(self, task):
        if not len(self.selectedTasks):
            self.controlSelectTask(task)
            return

        tasks = self.tasks
        if self.root.sortComboBox.currentText() == "Name":
            tasks = sorted(tasks, key=lambda x: x.asset.path + x.name)
        if self.root.sortComboBox.currentText() == "Last update":
            tasks = sorted(tasks, key=lambda x: x.updated)

        i1 = tasks.index(self.selectedTasks[-1])
        i2 = tasks.index(task)
        if i2 < i1:
            i1, i2 = i2, i1
        liste = tasks[i1:i2+1]
        for task in liste:
            if task not in self.selectedTasks:
                widgets = self.taskDico[task.uuid]
                for widget in widgets[:3]:
                    widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
                self.selectedTasks.append(task)
        self.changeTask()

    def controlSelectTask(self, task):
        if task not in self.selectedTasks:
            self.selectedTasks.append(task)
            widgets = self.taskDico[task.uuid]
            for widget in widgets[:3]:
                widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
            self.changeTask()

    def singleSelectTask(self, task):
        for st in self.selectedTasks:
            widgets = self.taskDico[st.uuid]
            for widget in widgets[:3]:
                widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % widgets[3])
            widgets[2].setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % widgets[4])
        self.selectedTasks = [task]
        widgets = self.taskDico[task.uuid]
        for widget in widgets[:3]:
            widget.setStyleSheet('background: %s; border-radius: 0px; padding: 0 0 0 0' % self.selColor['selection'])
        self.changeTask()

    def changeAsset(self):
        self.root.widgetMultiTask.hide()
        self.root.widgetOneTask.hide()
        self.root.widgetShot.hide()

        self.selectedAssets.sort(key=lambda k: k.path)
        if len(self.selectedAssets) == 1:
            self.root.widgetShot.show()
            self.root.widgetOneTask.show()
            tasks = sorted([x.name for x in self.selectedAssets[0].tasks])
            self.auto = True
            self.root.shotTaskComboBox.clear()
            self.root.shotTaskComboBox.addItems(tasks)
            self.auto = False
            if self.preferredTask is not None and self.preferredTask in tasks:
                self.root.shotTaskComboBox.setCurrentIndex(tasks.index(self.preferredTask))
            else:
                self.preferredTask = self.root.shotTaskComboBox.currentText()
            self.shotTaskChanged()
            self.assignedTags = {}
            clearLayout(self.root.assetTagLayout)
            for tag in self.selectedAssets[0].tags:
                self.addAssetTag(tag)
        else:
            tasks = sorted(list(set(sum([[y.name for y in x.tasks] for x in self.selectedAssets], []))))
            self.auto = True
            self.root.multiPreferredTaskComboBox.clear()
            self.root.multiPreferredTaskComboBox.addItems(tasks)
            self.auto = False
            if self.preferredTask is not None and self.preferredTask in tasks:
                self.root.multiPreferredTaskComboBox.setCurrentIndex(tasks.index(self.preferredTask))
            else:
                self.preferredTask = self.root.multiPreferredTaskComboBox.currentText()
            self.selectedTasks = sum([[y for y in x.tasks if y.name == self.preferredTask] for x in self.selectedAssets],[])
            self.changeTask()

    def shotPreferredTaskChanged(self):
        if not self.auto:
            self.preferredTask = self.root.multiPreferredTaskComboBox.currentText()
            self.selectedTasks = sum([[y for y in x.tasks if y.name == self.preferredTask] for x in self.selectedAssets], [])
            self.changeTask(multi=True)

    def shotTaskChanged(self):
        if not self.auto:
            self.preferredTask = self.root.shotTaskComboBox.currentText()
            tasks = [x.name for x in self.selectedAssets[0].tasks]
            if self.preferredTask is not None and self.preferredTask in tasks:
                i = tasks.index(self.preferredTask)
                self.selectedTasks = [self.selectedAssets[0].tasks[i]]
            self.changeTask()

    def checkRefreshTask(self):
        if not len(self.selectedTasks):
            self.WTimer.stop()
            return
        task = self.selectedTasks[0]
        if task.refresh():
            self.root.warningButton.setStyleSheet(
                "QPushButton{background: #663333} QPushButton::hover{background: #773333}")
        else:
            self.root.warningButton.setStyleSheet("QPushButton::!hover {background: transparent}")

    def changeTask(self, multi=False):
        width = 160
        height = width * 9 / 16
        self.root.widgetMultiTask.hide()
        self.root.widgetOneTask.hide()
        self.root.widgetShot.hide()
        self.commentEvents = []
        self.versionEvents = []
        showMarkRead = False

        if self.WTimer is not None:
            self.WTimer.stop()
        self.selectedTasks.sort(key=lambda k: k.asset.path+':'+k.name)
        if len(self.selectedTasks) == 1 and multi is False:
            # show task version, comment, vignette
            task = self.selectedTasks[0]
            task.refresh()
            self.root.warningButton.setStyleSheet("QPushButton::!hover {background: transparent}")

            self.WTimer = QtCore.QTimer()
            self.WTimer.timeout.connect(self.checkRefreshTask)
            self.WTimer.start(5000)

            self.root.pathLabel.setText(task.asset.path.split(':', 1)[-1])

            self.root.widgetOneTask.show()
            if len(self.selectedAssets):
                self.root.widgetShot.show()
            self.assignedUsers = {}
            clearLayout(self.root.assignedLayout)
            path = ':'.join(['path', task.asset.path, task.name])
            for user in self.currentProject.users:
                if user.match(path):
                    self.addAssigned(user.name, update=False)
                    break

            clearLayout(self.root.versionLayout.layout())
            for i, version in enumerate(sorted(task.versions, key=lambda k: -k.id)):
                fm = QtGui.QFontMetrics(QtGui.QFont('Whitney', pointSize=12, weight=12))
                smax = 0
                for comment in version.comments:
                    smax = max(smax, fm.width(comment.user)+8)

                h = QtWidgets.QHBoxLayout()
                p = QtWidgets.QPushButton()
                if os.path.exists(version.thumbnail):
                    p.setIcon(QtGui.QIcon(version.thumbnail))
                    p.setStyleSheet('background: transparent; border-radius: 0px')
                else:
                    p.setIcon(icon(self, "slash-white"))
                    p.setStyleSheet('background: black; border-radius: 0px')
                p.setIconSize(QtCore.QSize(width, height))
                p.setFixedSize(width, height)
                p.setFlat(True)

                p2 = PlayPushButton(self, version, p)
                p2.setStyleSheet("""QPushButton::hover{background: rgba(0,0,0,170); background-repeat: no-repeat;
                    background-position: center; background-image: url(:/icons/play-white-circle.svg);
                    border-radius: 0px}
                    QPushButton::!hover{background:  transparent;
                    border-radius: 0px} """.replace('url(:', 'url(' + self.shaDirectory))
                p2.setFixedSize(width, height)

                c = QtWidgets.QComboBox()
                c.setMaximumHeight(28)
                liste = sorted(self.currentProject.states.keys())
                if version.status not in liste:
                    liste.append(version.status)
                c.addItems(liste)
                c.setCurrentIndex(liste.index(version.status))
                c.currentIndexChanged.connect(lambda n, ver=version, combo=c: self.statusChanged(ver, combo))
                self.statusColor(c)
                user = QtWidgets.QLabel(version.user)
                user.setStyleSheet('color: %s' % self.selColor['white'])
                t = datetime.strptime(version.date, "%y/%m/%d %H:%M:%S")
                delta = (datetime.today()-t).days
                day = datetime.strftime(t, "%d/%m/%y")
                if delta == 0:
                    day = 'Today'
                elif delta == 1:
                    day = 'Yesterday'
                elif delta < 7:
                    day = datetime.strftime(t, "Last %A")
                hour = datetime.strftime(t, "%H:%M")
                date = QtWidgets.QLabel(day + ' at ' + hour)
                date.setStyleSheet("color: %s; font: 10pt 'Whitney Light'" % self.selColor['grey'])
                sup = QtWidgets.QPushButton()
                sup.setIcon(icon(self, "trash-white"))
                sup.setIconSize(QtCore.QSize(28, 28))
                sup.setFixedWidth(20)
                sup.setFixedHeight(28)
                sup.setFlat(True)
                sup.setStyleSheet('QPushButton::!hover{background: transparent}')
                if version.user != os.getenv('USER'):
                    sup.setEnabled(False)
                    sup.setIcon(icon(self, "trash-grey"))

                commentWidget = QtWidgets.QWidget()
                cv = QtWidgets.QVBoxLayout()
                cv.setContentsMargins(0, 0, 0, 0)
                commentWidget.setLayout(cv)

                plus = QtWidgets.QPushButton()
                plus.setIcon(icon(self, "plus-white"))
                plus.setIconSize(QtCore.QSize(24, 24))
                color = self.selColor['edit'] if i % 2 else self.selColor['comment2']
                plus.clicked.connect(lambda cvl=cv, ver=version, s=smax, col=color, master=cv: self.addComment(cvl, ver, s, col, master))
                plus.setFixedWidth(45)
                plus.setFixedHeight(28)
                plus.setStyleSheet('QPushButton::!hover{ background: transparent}')

                sho = QtWidgets.QPushButton('%s' % len(version.comments))
                sho.setIcon(icon(self, "chevron-up"))
                sho.setIconSize(QtCore.QSize(24, 24))
                sho.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
                sho.setFixedHeight(28)
                sho.setStyleSheet('QPushButton{padding} QPushButton::!hover{ background: transparent}')
                sho.clicked.connect(lambda cwd=commentWidget, s=sho, ver=version: self.toggleComment(cwd, s, ver))

                h.addWidget(p)
                h.addWidget(user, alignment=QtCore.Qt.AlignBottom)
                h.addWidget(date, alignment=QtCore.Qt.AlignBottom)
                h.addStretch()
                h.addWidget(plus, alignment=QtCore.Qt.AlignBottom)
                h.addWidget(sho, alignment=QtCore.Qt.AlignBottom)
                h.addWidget(c, alignment=QtCore.Qt.AlignBottom)
                h.addWidget(sup, alignment=QtCore.Qt.AlignBottom)
                mainWidget = QtWidgets.QGroupBox('v%s' % str(version.id).zfill(3))
                v = QtWidgets.QVBoxLayout()
                v.addLayout(h)
                mainWidget.setLayout(v)
                h.setContentsMargins(4, 0, 0, 0)
                v.setContentsMargins(6, 0, 0, 0)
                self.root.versionLayout.layout().addWidget(mainWidget)
                sup.clicked.connect(lambda ver=version, ly=mainWidget: self.deleteVersion(ver, ly))

                v.addWidget(commentWidget)

                line = QtWidgets.QFrame()
                line.setFrameShape(QtWidgets.QFrame.HLine)
                line.setFrameShadow(QtWidgets.QFrame.Sunken)
                self.root.versionLayout.layout().addWidget(line)

                if i > 0:
                    self.toggleComment(commentWidget, sho, version, force=True)

                C = []
                for j, comment in enumerate(version.comments):
                    color = self.selColor['edit'] if j % 2 else self.selColor['comment2']
                    Ce, Ca, Cl, Cs, CL, Co, Cw = self.addCommentLine(cv, comment, smax, color)
                    C.append([comment, Cw, Ce, Co])
                    if os.getenv('USER') not in comment.witness:
                        showMarkRead = True

                mainWidget.setAttribute(QtCore.Qt.WA_StyledBackground, True)
                if os.getenv('USER') not in version.witness:
                    mainWidget.setStyleSheet('QGroupBox{border: 2px solid %s}' % self.selColor['selectLight'])
                    myevent = VersionEventFilter(self, version, mainWidget, C)
                    mainWidget.installEventFilter(myevent)
                    self.versionEvents.append(myevent)
                    showMarkRead = True

                v.setContentsMargins(0, 0, 0, 0)
            self.root.readTaskButton.setVisible(showMarkRead)
            self.root.versionLayout.layout().addStretch()
            c = self.root.versionLayout.layout().count()
            for i, y in enumerate(range(c)):
                self.root.versionLayout.layout().setStretch(i, i == c-1)

        elif len(self.selectedTasks) > 1 or multi:
            # show list of tasks with version, assigned, what else ?
            self.root.widgetMultiTask.show()
            self.root.taskTableWidget.clear()
            self.root.taskTableWidget.setRowCount(len(self.selectedTasks))
            self.root.taskTableWidget.setColumnCount(5)
            self.root.taskTableWidget.setColumnWidth(1, 160)
            self.root.taskTableWidget.setHorizontalHeaderLabels(['Asset', 'Task', 'Version', 'Last status', 'Assigned To'])
            for i, task in enumerate(self.selectedTasks):
                item = QtWidgets.QTableWidgetItem(' '.join(task.asset.path.split(':')[1:]))
                item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
                self.root.taskTableWidget.setItem(i, 0, item)

                item = QtWidgets.QTableWidgetItem(task.name)
                item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
                self.root.taskTableWidget.setItem(i, 1, item)

                c = QtWidgets.QComboBox()
                c.addItems(['v'+str(x.id).zfill(3) for x in reversed(task.versions)])
                self.root.taskTableWidget.setCellWidget(i, 2, c)

                if len(task.versions):
                    c = QtWidgets.QComboBox()
                    liste = sorted(self.currentProject.states.keys())
                    c.addItems(liste)
                    vi = liste.index(task.versions[-1].status)
                    c.setCurrentIndex(vi)
                    self.root.taskTableWidget.setCellWidget(i, 3, c)
                    c.setStyleSheet('''QComboBox { background-color: %s}
                    QComboBox QAbstractItemView { background-color: %s}
                    ''' % (self.currentProject.states[task.versions[-1].status], self.selColor['dark']))
                    c.activated.connect(lambda e, row=i: self.multiStatusChange(e, row))

                w = QtWidgets.QWidget()
                h = QtWidgets.QHBoxLayout()
                h.setContentsMargins(0, 0, 0, 0)
                h.setSpacing(1)
                w.setLayout(h)

                for user in self.currentProject.users:
                    path = ':'.join(['path', task.asset.path, task.name])
                    assignment = user.match(path)
                    if assignment is not False:
                        p = QtWidgets.QPushButton(user.name)
                        p.setStyleSheet('''QPushButton{background: %s}
                                           QPushButton::hover{background: %s}''' % (self.selColor['selection'],
                                                                                    self.selColor['selectLight']))
                        p.clicked.connect(lambda t=task, u=user.name: self.delMultiAssigned(t, u))
                        h.addWidget(p)
                        if assignment != path:
                            p.setEnabled(False)

                h.addStretch()
                p = QtWidgets.QPushButton()
                p.setIcon(icon(self, 'plus-white'))
                menu = QtWidgets.QMenu(p)
                edit = QtWidgets.QLineEdit()
                wac = QtWidgets.QWidgetAction(menu)
                wac.setDefaultWidget(edit)
                menu.addAction(wac)
                p.setMenu(menu)
                edit.setFocus(QtCore.Qt.OtherFocusReason)
                p.setStyleSheet('QPushButton::menu-indicator{width:0px;}')

                # installe le QCompleter Filter
                completer = QtWidgets.QCompleter(edit)
                completer.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
                edit.setCompleter(completer)
                # installe le model à partir de la liste fournie datas
                # installe le QSortFilterProxyModel qui s'insère entre le QCompleter et le model
                proxymodel = QtCore.QSortFilterProxyModel(edit)
                proxymodel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
                proxymodel.setSourceModel(self.amodel)
                completer.setModel(proxymodel)
                # chaque changement de texte déclenchera la filtration du model
                edit.textEdited.connect(proxymodel.setFilterFixedString)
                edit.returnPressed.connect(lambda e=edit, m=menu, t=task: self.addMultiAssigned(e, m, t))

                h.addWidget(p)
                self.root.taskTableWidget.setCellWidget(i, 4, w)

    def multiStatusChange(self, ind, row):
        if self.auto:
            return
        self.auto = True
        stat = sorted(self.currentProject.states.keys())[ind]
        rows = self.root.taskTableWidget.selectionModel().selectedRows()
        rows = [x.row() for x in rows]
        if row not in rows:
            rows.append(row)
        for i in rows:
            task = self.selectedTasks[i]
            widget = self.root.taskTableWidget.cellWidget(i, 3)
            if widget is not None:
                widget.setCurrentIndex(ind)
                task.versions[-1].setStatus(stat)
                widget.setStyleSheet('''QComboBox { background-color: %s}
                                    QComboBox QAbstractItemView { background-color: %s}
                                    ''' % (self.currentProject.states[task.versions[-1].status], self.selColor['dark']))
        self.auto = False

    def addMultiAssigned(self, edit, menu, task):
        text = edit.text()
        edit.clear()
        menu.hide()
        tasks = [task]
        rows = self.root.taskTableWidget.selectionModel().selectedRows()
        if len(rows):
            tasks = []
            for i in rows:
                tasks.append(self.selectedTasks[i.row()])
        user = self.currentProject.getUser(text)
        if user is None:
            user = self.currentProject.addUser(text)

        for task in tasks:
            path = ':'.join(['path', task.asset.path, task.name])
            user.addAssign(path)
        self.changeTask()
        self.root.taskTableWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        for i in rows:
            self.root.taskTableWidget.selectRow(i.row())
        self.root.taskTableWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

    def delMultiAssigned(self, task, user):
        tasks = [task]
        rows = self.root.taskTableWidget.selectionModel().selectedRows()
        if len(rows):
            tasks = []
            for i in rows:
                tasks.append(self.selectedTasks[i.row()])
        user = self.currentProject.getUser(user)
        if user is not None:
            for task in tasks:
                path = ':'.join(['path', task.asset.path, task.name])
                user.removeAssign(path)
        self.changeTask()
        self.root.taskTableWidget.setSelectionMode(QtWidgets.QAbstractItemView.MultiSelection)
        for i in rows:
            self.root.taskTableWidget.selectRow(i.row())
        self.root.taskTableWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

    def statusChanged(self, version, combo):
        version.setStatus(combo.currentText())
        self.statusColor(combo)

    def statusColor(self, combo):
        combo.setStyleSheet('''QComboBox{background: %s}
            QComboBox::hover{background-color: %s;}''' % (self.currentProject.states[combo.currentText()], self.selColor['selection']))

    def deleteVersion(self, version, layout):
        result = QtWidgets.QMessageBox.question(self.root,
                                                'Confirm', 'Are you sure to delete version %s ?' % version.id,
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                QtWidgets.QMessageBox.No)
        if result == QtWidgets.QMessageBox.Yes:
            version.delete()
            clearLayout(layout.layout())
            layout.deleteLater()

    def clearPlaylist(self):
        self.playlist = []
        self.root.playlistComboBox.setCurrentIndex(0)
        self.showPlaylist()
        self.setPlaylistButtons()

    def addToPlaylist(self, version):
        self.playlist.append(version)
        self.showPlaylist()
        self.setPlaylistButtons()
        self.showPlayListWidget()

    def addAllToPlaylist(self):
        for i, task in enumerate(self.selectedTasks):
            if len(task.versions):
                widget = self.root.taskTableWidget.cellWidget(i, 2)
                if widget is not None:
                    version = widget.currentText()
                    version = int(version[1:])
                    self.playlist.append([x for x in task.versions if x.id == version][0])
        self.showPlaylist()
        self.setPlaylistButtons()
        self.showPlayListWidget()

    def showPlaylist(self):
        for item in self.view.scene.items():
            self.view.scene.removeItem(item)
        for i, version in enumerate(self.playlist):
            b = BinNode(self.view, version)
            b.setPos(i * 85, -10)
            b.realx = i * 85
        self.view.scene.setSceneRect(-3, 0, max(len(self.view.scene.items()) * 85, self.view.geometry().width()), 40)
        self.view.centerOn(0, 0)

    def removeFromPlaylist(self, obj):
        for item in self.view.scene.items():
            if item.realx > obj.realx:
                item.setX(item.realx-85)
        self.view.scene.removeItem(obj)
        self.playlist.remove(obj.version)
        self.setPlaylistButtons()

    def setPlaylistButtons(self):
        if len(self.playlist) == 0:
            self.root.savePlaylistPushButton.setEnabled(False)
            self.root.deletePlaylistPushButton.setEnabled(False)
            self.root.savePlaylistPushButton.setIcon(icon(self, 'save-grey'))
            self.root.deletePlaylistPushButton.setIcon(icon(self, 'trash-grey'))
        else:
            for i, playlist in enumerate(self.currentProject.playlists):
                if playlist.versions == self.playlist:
                    self.root.deletePlaylistPushButton.setEnabled(True)
                    self.root.savePlaylistPushButton.setEnabled(False)
                    self.root.savePlaylistPushButton.setIcon(icon(self, 'save-grey'))
                    self.root.deletePlaylistPushButton.setIcon(icon(self, 'trash-white'))
                    self.root.playlistComboBox.setCurrentIndex(i + 1)
                    break
            else:
                self.root.deletePlaylistPushButton.setEnabled(False)
                self.root.savePlaylistPushButton.setEnabled(True)
                self.root.savePlaylistPushButton.setIcon(icon(self, 'save-white'))
                self.root.deletePlaylistPushButton.setIcon(icon(self, 'trash-grey'))
                self.root.playlistComboBox.setCurrentIndex(0)

    @staticmethod
    def playMovie(version):
        more = ''
        if len(version.lut):
            more = " -llut %s" % version.lut
        options = "-yuv -l -play -fps 24 -lram 6 -rthreads 8 -lookback 2"
        rv = "vglrun /mnt/BIN/RV/rv-Linux-x86-64-7.2.1/bin/rv"
        cmd = "{0} {1}{2} {3}".format(rv, options, more, version.movie)
        FNULL = open(os.devnull, 'w')
        subprocess.Popen(cmd, shell=True, stdout=FNULL)

    @staticmethod
    def playSequence(version):
        more = ''
        if len(version.lut):
            more = " -llut %s" % version.lut
        options = "-yuv -l -play -fps 24 -lram 6 -rthreads 8 -lookback 2"
        rv = "vglrun /mnt/BIN/RV/rv-Linux-x86-64-7.2.1/bin/rv"
        cmd = "{0} {1}{2} {3}".format(rv, options, more, version.sequence)
        FNULL = open(os.devnull, 'w')
        subprocess.Popen(cmd, shell=True, stdout=FNULL)

    def playPlaylistSequence(self):
        cmd = '/bin/vglrun /mnt/BIN/RV/rv-Linux-x86-64-7.2.1/bin/rv'
        FNULL = open(os.devnull, 'w')
        for version in self.playlist:
            cmd += ' ' + version.sequence
        subprocess.Popen([cmd], shell=True, stdout=FNULL)

    def playPlaylistMovie(self):
        cmd = '/bin/vglrun /mnt/BIN/RV/rv-Linux-x86-64-7.2.1/bin/rv'
        FNULL = open(os.devnull, 'w')
        for version in self.playlist:
            cmd += ' ' + version.movie
        subprocess.Popen([cmd], shell=True, stdout=FNULL)

    def toggleComment(self, widget, s, version, force=None):
        s.setStyleSheet('')
        if widget.isVisible() and force is None or force:
            widget.hide()
            s.setIcon(icon(self, "chevron-down"))
            if os.getenv('USER') in version.witness:
                for comment in version.comments:
                    if os.getenv('USER') not in comment.witness:
                        s.setStyleSheet('border: 1px solid %s' % self.selColor['selection'])
                        break
        else:
            widget.show()
            s.setIcon(icon(self, "chevron-up"))

    @staticmethod
    def setHeight(ptxt,  nrows):
        pdoc = ptxt.document()
        fm = QtGui.QFontMetrics(pdoc.defaultFont())
        margins = ptxt.contentsMargins()
        nHeight = fm.lineSpacing() * nrows + (
                    pdoc.documentMargin() + ptxt.frameWidth()) * 2 + margins.top() + margins.bottom()
        ptxt.setFixedHeight(nHeight)

    def addCommentLine(self, layout, comment, smax, color):
        mainWidget = QtWidgets.QWidget()
        newL = QtWidgets.QHBoxLayout()
        mainWidget.setLayout(newL)

        u = QtWidgets.QPushButton(comment.user)
        u.setFlat(True)
        u.setToolTip(comment.date)
        u.setFixedWidth(smax)
        u.setStyleSheet('QPushButton{background: transparent; text-align: left}')
        #u.setStyleSheet('''QPushButton{background: transparent; text-align: left}
        #                QPushButton::hover{color: %s}''' % self.selColor['selection'])
        #u.clicked.connect(lambda c=comment, b=u: self.changeUser(c, b))

        edit = CommentEdit()
        edit.setReadOnly(True)

        if comment.flag not in self.colorDic.keys():
            comment.setFlag('Artist')

        if comment.flag != 'Artist':
            color = self.colorDic[comment.flag]
        edit.setStyleSheet('border-radius:0; background-color: %s' % color)

        newL.addWidget(u)

        attachHeight = 30
        vComment = QtWidgets.QVBoxLayout()
        vComment.addWidget(edit)
        cScroll = QtWidgets.QScrollArea()
        if not len(comment.attachments):
            cScroll.hide()
        cScroll.setWidgetResizable(1)
        cScroll.setMaximumHeight(cScroll.horizontalScrollBar().sizeHint().height() + attachHeight)
        cScroll.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        cScroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        w = QtWidgets.QWidget()
        hComment = QtWidgets.QHBoxLayout(w)
        hComment.setContentsMargins(0, 0, 0, 0)
        cScroll.setWidget(w)
        self.fillAttachments(hComment, comment, attachHeight, cScroll)
        vComment.addWidget(cScroll)
        newL.addLayout(vComment)

        editable = (comment.user == os.getenv('USER'))
        myevent = CommentEventFilter(self, comment, edit, color, attachHeight, hComment, cScroll, editable, mainWidget, layout)
        edit.viewport().installEventFilter(myevent)
        self.commentEvents.append(myevent)

        sup = QtWidgets.QPushButton()
        sup.setIconSize(QtCore.QSize(20, 20))
        sup.setFixedWidth(20)
        sup.setFlat(True)
        sup.setStyleSheet('QPushButton::!hover{background: transparent}')
        sup.clicked.connect(lambda c=comment, lay=newL, master=layout: self.deleteComment(c, lay, True, master))
        newL.addWidget(sup)
        if comment.user != os.getenv('USER'):
            sup.setEnabled(False)
            sup.setIcon(icon(self, "trash-grey"))
        else:
            sup.setIcon(icon(self, "trash-white"))

        layout.insertWidget(0, mainWidget)
        edit.setHtml(comment.text)

        mainWidget.setAttribute(QtCore.Qt.WA_StyledBackground, True)
        mainWidget.setObjectName('toto')
        obj = mainWidget.objectName()
        if os.getenv('USER') not in comment.witness and os.getenv('USER') in comment.taskVersion.witness:
            mainWidget.setStyleSheet('QWidget#%s{border: 1px solid %s}' % (obj, self.selColor['selectLight']))
        newL.setContentsMargins(0, 0, 0, 0)
        return edit, attachHeight, hComment, cScroll, newL, color, mainWidget

    def changeUser(self, comment, button):
        menu = QtWidgets.QMenu(button)
        menu.setStyleSheet(self.style)

        for user in sorted(self.currentProject.users, key=lambda x: x.name):
            if user.name != button.text():
                action = QtWidgets.QAction(user.name, menu)
                menu.addAction(action)

        result = menu.exec_(button.mapToGlobal(QtCore.QPoint(0, 0)))
        if result:
            button.setText(result.text())
            comment.setUser(result.text())

    def showSummary(self):
        tasks = []
        self.toMark = []
        width = 80
        height = width * 9 / 16
        listW = self.root.summaryTableWidget
        listW.setColumnWidth(0, width)
        listW.setColumnWidth(1, 350)
        listW.setFixedWidth(width + 350)

        tasks = self.combineFilters(self.currentProject.assets, self.f2.filters, 'task')
        tasks = sorted(tasks, key=lambda x: -x.updated)
        listW.setRowCount(len(tasks))
        self.readButton.hide()
        for i, task in enumerate(tasks):
            listW.setRowHeight(i, height)
            button = QtWidgets.QPushButton()

            labv = SummaryButton(self, '')
            labc = SummaryButton(self, '')
            label = SummaryButton(self, task.asset.path.split(':', 1)[-1] + ' - ' + task.name)
            label.setWidgets(labv, labc)
            labv.setWidgets(label, labc)
            labc.setWidgets(labv, label)

            last = [None, -1, 0]
            witnessv = 0
            witnessc = 0
            for version in task.versions:
                if os.getenv('USER') not in version.witness:
                    witnessv += 1
                for comment in version.comments:
                    if os.getenv('USER') not in comment.witness:
                        witnessc += 1
                        break
                if version.date > last[1]:
                    last = [version, version.date, None, version.thumbnail]
                for c in version.comments:
                    if c.date > last[1]:
                        last = [c.taskVersion, c.date, c, c.taskVersion.thumbnail]
            if last[0] is not None:
                if os.path.exists(last[3]):
                    button.setIcon(QtGui.QIcon(last[3]))
                    button.setStyleSheet('background: black; border-radius: 0px')
                else:
                    button.setIcon(icon(self, "slash-white"))
                    button.setStyleSheet('background: black; border-radius: 0px')
            else:
                button.setIcon(icon(self, "slash-white"))
                button.setStyleSheet('background: black; border-radius: 0px')

            button.setIconSize(QtCore.QSize(width, height))
            button.setFixedSize(width, height)
            button.setFlat(True)

            label.clicked.connect(lambda v=task: self.goto(v))
            labv.clicked.connect(lambda v=task: self.goto(v))
            labc.clicked.connect(lambda v=task: self.goto(v))
            labw = QtWidgets.QWidget()
            labl = QtWidgets.QHBoxLayout()
            labl.setContentsMargins(0, 0, 12, 0)
            labl.setSpacing(0)
            labw.setLayout(labl)
            if witnessv:
                labv.setText(str(witnessv))
                labv.setIcon(icon(self, 'version'))
            if witnessc:
                labc.setText(str(witnessc))
                labc.setIcon(icon(self, 'comment'))
            if witnessv+witnessc == 0:
                if last[0] is not None:
                    label.setColor(self.selColor['dark'])
                    labv.setColor(self.selColor['dark'])
                    labc.setColor(self.selColor['dark'])
                else:
                    label.setColor('#202020')
                    labv.setColor('#202020')
                    labc.setColor('#202020')
            else:
                if len(self.toMark) == 0:
                    self.readButton.show()
                self.toMark.append([task, label, labv, labc])
                label.setColor(self.selColor['edit'])
                labv.setColor(self.selColor['edit'])
                labc.setColor(self.selColor['edit'])

            label.setMinimumHeight(height)
            labv.setMinimumHeight(height)
            labc.setMinimumHeight(height)
            labl.addWidget(label)
            labl.addWidget(labv)
            labl.addWidget(labc)
            labl.setStretch(0, 1)

            listW.setCellWidget(i, 0, button)
            listW.setCellWidget(i, 1, labw)

            self.app.processEvents()

    def goto(self, task):
        index = self.root.patternComboBox.findText(task.asset.path.split(':')[0],
                                                   flags=QtCore.Qt.MatchFixedString)
        self.f1.reset()

        self.selectedAssets = [task.asset]
        self.root.patternComboBox.setCurrentIndex(index)
        self.assets = [task.asset.path]
        self.filterList()
        self.tasks = [task]

        self.selectedTasks = [task]
        self.preferredTask = task.name
        self.showDesk()
        self.changeAsset()

    def markAllRead(self):
        for mark in self.toMark:
            mark[1].setColor(self.selColor['dark'])
            mark[2].hide()
            mark[3].hide()
            for version in mark[0].versions:
                version.addWitness(os.getenv('USER'))
                for comment in version.comments:
                    comment.addWitness(os.getenv('USER'))
        self.changeTask()

    def markTaskRead(self):
        task = self.selectedTasks[0]
        for mark in self.toMark:
            if mark[0] is task:
                mark[1].setColor(self.selColor['dark'])
                mark[2].hide()
                mark[3].hide()
        for version in task.versions:
            version.addWitness(os.getenv('USER'))
            for comment in version.comments:
                comment.addWitness(os.getenv('USER'))
        self.changeTask()

    def fillAttachments(self, layout, comment, attachHeight, scroll):
        clearLayout(layout)
        if not len(comment.attachments):
            scroll.hide()
        else:
            scroll.show()
        elements = list(comment.attachments)
        if len(comment.link):
            elements.append('link:%s' % comment.link)
        for att in elements:
            p = QtWidgets.QPushButton()
            if att.startswith('link:'):
                p.setIcon(icon(self, 'link'))
            else:
                p.setIcon(QtGui.QIcon(att))

            p.setIconSize(QtCore.QSize(attachHeight * 16 / 9, attachHeight))
            p.setFixedSize(attachHeight * 16 / 9, attachHeight)
            p1 = QtWidgets.QPushButton(p)
            p1.setToolTip(att)
            p1.setFixedSize(attachHeight * 16 / 9, attachHeight)
            p1.clicked.connect(lambda path=att: self.viewAttachment(path))
            p1.setStyleSheet("""QPushButton::!hover{background: rgba(0,0,0,0);}
                QPushButton::hover{background: rgba(130,80,168,128);}""")
            layout.addWidget(p)
        if len(elements):
            layout.addStretch()

    def editComment(self, comment, edit, color, attachHeight, layout, scroll, master):
        self.commentWindow = CommentUI(self, comment, edit, color, attachHeight, layout, scroll, master)

    @staticmethod
    def viewAttachment(path):
        if path.startswith('link:'):
            cmd = '/bin/vglrun %s' % path[5:]
        else:
            cmd = '/bin/vglrun /mnt/BIN/RV/rv-Linux-x86-64-7.2.1/bin/rv "%s"' % path
        FNULL = open(os.devnull, 'w')
        subprocess.Popen([cmd], shell=True, stdout=FNULL)

    def addComment(self, layout, version, smax, color, master):
        comment = version.addComment()
        edit, attachHeight, layout, scroll, newL, i, j = self.addCommentLine(layout, comment, smax, color)
        self.commentWindow = CommentUI(self, comment, edit, color, attachHeight, layout, scroll, master, first=True,
                                       newL=newL)

    def deleteComment(self, comment, layout, confirm, master):
        result = QtWidgets.QMessageBox.Yes
        if confirm:
            result = QtWidgets.QMessageBox.question(self.root, 'Confirm', 'Are you sure to delete this comment ?',
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)
        if result == QtWidgets.QMessageBox.Yes:
            comment.delete()
            clearLayout(layout)
            for i in range(master.count()):
                layout_item = master.itemAt(i)
                if layout_item.widget().layout() == layout:
                    master.removeItem(layout_item)
                    break

    def showStats(self):
        if self.root.sumTabWidget.tabText(self.root.sumTabWidget.currentIndex()) == "Stats":
            clearLayout(self.root.statsTab.layout())
            self.statScene = QtWidgets.QGraphicsScene()
            self.statGraph = StatView(self)
            self.statGraph.setScene(self.statScene)
            tasks = sum([x.tasks for x in self.currentProject.assets if x.omit is False], [])
            i = 0
            for k in self.currentProject.availableTasks:
                stasks = [x for x in tasks if x.name.startswith(k)]
                if len(stasks):
                    item1 = Pie(self, stasks, k, 60)
                    item1.setPos(0, 80+(60*2+20)*i)
                    self.statScene.addItem(item1)
                    i += 1
            self.statCount = [i, 80+(60*2+20)*(i+1)]
            self.statScene.setSceneRect(0, 0, 300, 80+(60*2+20)*(i+1))
            self.root.statsTab.layout().addWidget(self.statGraph)

    def selectStat(self, taskname, status):
        for text in list(self.f1.filters.keys()):
            self.f1.filters[text].deleteLater()
            del self.f1.filters[text]
        self.f1.addFilter('task:' + taskname, refresh=False)
        self.f1.addFilter('status:' + status, refresh=False)

        self.f1.root.deleteFilterPushButton.setEnabled(False)
        self.f1.root.deleteFilterPushButton.setIcon(icon(self, 'trash-grey'))

        self.f1.root.addFilterPushButton.setEnabled(True)
        self.f1.root.addFilterPushButton.setIcon(icon(self, 'save-white'))

        self.filterList()
        self.auto = True
        self.root.assetsListWidget.selectAll()
        self.auto = False
        self.setShotFilter()
        self.filterDesk()

    def timeLog(self):
        self.timeLogWindow = TimeLogUI(self)


class PlayButton(QtWidgets.QPushButton):
    def __init__(self, gui, parent=None):
        super(PlayButton, self).__init__(parent)
        self.gui = gui

    def mouseReleaseEvent(self, event):
        QtWidgets.QPushButton.mouseReleaseEvent(self, event)
        if event.modifiers() == QtCore.Qt.ControlModifier:
            self.gui.playPlaylistSequence()
        else:
            self.gui.playPlaylistMovie()


class TagButton(QtWidgets.QPushButton):
    def __init__(self, gui, tag, parent=None):
        super(TagButton, self).__init__(parent)
        self.gui = gui
        self.tag = tag
        self.setText(tag)
        self.setStyleSheet('''QPushButton{background: %s}
                QPushButton::hover{background: %s}''' % (self.gui.selColor['selection'],
                                                         self.gui.selColor['selectLight']))

    def mouseReleaseEvent(self, event):
        QtWidgets.QPushButton.mouseReleaseEvent(self, event)
        if event.modifiers() == QtCore.Qt.ControlModifier:
            self.gui.removeAssetTag(self.tag)


class FilterButton(QtWidgets.QPushButton):
    def __init__(self, gui, text, inverted, parent=None):
        super(FilterButton, self).__init__(parent)
        self.gui = gui
        self.text = text
        self.inverted = inverted
        self.setText(text)
        self.setStyleSheet('''QPushButton{background: %s}
                QPushButton::hover{background: %s}''' % (self.gui.gui.selColor['selection'],
                                                         self.gui.gui.selColor['selectLight']))
        if inverted:
            self.setIcon(icon(self.gui.gui, 'slash-white'))
        self.setIconSize(QtCore.QSize(14, 14))

    def mouseReleaseEvent(self, event):
        QtWidgets.QPushButton.mouseReleaseEvent(self, event)
        if event.modifiers() == QtCore.Qt.ControlModifier:
            self.gui.removeFilter(self.text)
        else:
            self.gui.toggleFilter(self)


class UserButton(QtWidgets.QPushButton):
    def __init__(self, gui, user, parent=None):
        super(UserButton, self).__init__(parent)
        self.gui = gui
        self.user = user
        self.setText(user.name)
        self.setStyleSheet('''QPushButton{background: %s}
        QPushButton::hover{background: %s}''' % (self.gui.selColor['selection'],
                                                 self.gui.selColor['selectLight']))

    def mouseReleaseEvent(self, event):
        QtWidgets.QPushButton.mouseReleaseEvent(self, event)
        if event.modifiers() == QtCore.Qt.ControlModifier:
            task = self.gui.selectedTasks[0]
            path = ':'.join(['path', task.asset.path, task.name])
            assignment = self.user.match(path)
            if assignment == path:
                self.gui.removeAssigned(self.user, assignment)
            else:
                result = QtWidgets.QMessageBox.question(self, 'Warning',
                                                        'This assignment affect other assets. Confirm ?\n\n%s' % assignment,
                                                        QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                        QtWidgets.QMessageBox.No)
                if result == QtWidgets.QMessageBox.Yes:
                    self.gui.removeAssigned(self.user, assignment)
        else:
            task = self.gui.selectedTasks[0]
            path = ':'.join(['path', task.asset.path, task.name])
            assignment = self.user.match(path)
            menu = QtWidgets.QMenu(self)
            menu.setStyleSheet(self.gui.style)
            editAction = QtWidgets.QAction('Edit', menu)
            menu.addAction(editAction)
            result = menu.exec_(self.mapToGlobal(QtCore.QPoint(0, 0)))
            if result:
                if result == editAction:
                    name, ok = QtWidgets.QInputDialog.getText(self.gui.root, 'Change assignment', 'Assign :', text=assignment)
                    if ok:
                        assign = [x if x != assignment else name for x in self.user.assign]
                        self.user.setAssigns(assign)


class PlayPushButton(QtWidgets.QPushButton):
    def __init__(self, gui, version, parent=None):
        super(PlayPushButton, self).__init__(parent)
        self.gui = gui
        self.version = version

    def mouseReleaseEvent(self, event):
        if event.modifiers() == QtCore.Qt.ControlModifier:
            self.gui.addToPlaylist(self.version)
        elif event.modifiers() == QtCore.Qt.ShiftModifier:
            self.gui.playSequence(self.version)
        else:
            self.gui.playMovie(self.version)


class CommentEdit(QtWidgets.QTextEdit):
    def __init__(self, parent=None):
        super(CommentEdit, self).__init__(parent)

    def resizeEvent(self, event):
        QtWidgets.QTextEdit.resizeEvent(self, event)
        fm = QtGui.QFontMetrics(QtGui.QFontMetrics(QtGui.QFont('Whitney', pointSize=12, weight=12)))
        wwc = 0.0
        for line in self.toPlainText().split('\n'):
            wwc += int(fm.width(line) / self.document().size().width()) + 1
        self.setFixedHeight(wwc * fm.height() * 1.15 + 8)


class FilterObject(object):
    def __init__(self, gui, layout, id):
        self.gui = gui
        self.id = id
        self.filters = {}
        self.auto = False
        mainUIPath = os.path.join(self.gui.shaDirectory, 'UI', 'filter.ui')
        self.root = QtCompat.loadUi(mainUIPath)
        layout.addWidget(self.root)
        self.root.filtersComboBox.setSizeAdjustPolicy(QtWidgets.QComboBox.AdjustToContents)
        self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-white'))
        self.root.addFilterPushButton.setIcon(icon(self.gui, 'save-white'))
        self.root.deleteFilterPushButton.setStyleSheet('QPushButton::!hover{background: transparent}')
        self.root.addFilterPushButton.setStyleSheet('QPushButton::!hover{background: transparent}')

        self.root.addFilterPushButton.clicked.connect(self.saveFilter)
        self.root.deleteFilterPushButton.clicked.connect(self.deleteFilter)
        self.root.filtersComboBox.currentIndexChanged.connect(self.loadFilter)
        self.root.deleteFilterPushButton.setEnabled(False)
        self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-grey'))
        self.root.deleteFilterPushButton.setIconSize(QtCore.QSize(24, 24))
        self.root.addFilterPushButton.setIconSize(QtCore.QSize(24, 24))

        self.root.filterEdit.returnPressed.connect(self.completerActivated)

        # installe le QCompleter Filter #############################################################
        completer = QtWidgets.QCompleter(self.root.filterEdit)
        completer.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        self.root.filterEdit.setCompleter(completer)
        # installe le model à partir de la liste fournie datas
        self.model = QtGui.QStandardItemModel()
        # installe le QSortFilterProxyModel qui s'insère entre le QCompleter et le model
        proxymodel = QtCore.QSortFilterProxyModel(self.root.filterEdit)
        proxymodel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        proxymodel.setSourceModel(self.model)
        completer.setModel(proxymodel)
        # chaque changement de texte déclenchera la filtration du model
        self.root.filterEdit.textEdited.connect(proxymodel.setFilterFixedString)
        completer.activated.connect(self.completerActivated, QtCore.Qt.DirectConnection)

    def completerActivated(self):
        s = self.root.filterEdit.text()
        QtCore.QTimer.singleShot(0, self.root.filterEdit.clear)
        if len(s):
            self.addFilter(s)

    def addFilter(self, text, refresh=True):
        if ':' not in text or text.split(':')[0] not in ['tag', 'user', 'status', 'task']:
            text = 'tag:' + text
        if text not in self.filters.keys():
            newFilterButton = FilterButton(self, text, False)
            self.root.filterLayout.addWidget(newFilterButton)
            self.filters[text] = newFilterButton
            self.auto = True
            self.root.filtersComboBox.setCurrentIndex(0)
            self.auto = False

            self.root.deleteFilterPushButton.setEnabled(False)
            self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-grey'))
            self.root.addFilterPushButton.setEnabled(True)
            self.root.addFilterPushButton.setIcon(icon(self.gui, 'save-white'))
        if self.id == 'task':
            self.gui.filterList()
            self.gui.setShotFilter(refresh=False)
            self.gui.smartSelect()
            self.gui.setShotFilter(refresh)
        if self.id == 'summary':
            self.gui.showSummary()

    def removeFilter(self, text):
        self.filters[text].hide()
        self.filters[text].deleteLater()
        del self.filters[text]
        self.auto = True
        self.root.filtersComboBox.setCurrentIndex(0)
        self.auto = False
        self.root.deleteFilterPushButton.setEnabled(False)
        self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-grey'))
        self.root.addFilterPushButton.setEnabled(True)
        self.root.addFilterPushButton.setIcon(icon(self.gui, 'save-white'))
        if self.id == 'task':
            self.gui.filterList()
            self.gui.auto = True
            self.gui.smartSelect()
            self.gui.auto = False
            self.gui.setShotFilter()
        if self.id == 'summary':
            self.gui.showSummary()

    def toggleFilter(self, button):
        text = button.text
        self.auto = True
        self.root.filtersComboBox.setCurrentIndex(0)
        self.auto = False
        self.root.deleteFilterPushButton.setEnabled(False)
        self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-grey'))
        self.root.addFilterPushButton.setEnabled(True)
        self.root.addFilterPushButton.setIcon(icon(self.gui, 'save-white'))
        button.inverted = not button.inverted
        if button.inverted:
            button.setIcon(icon(self.gui, 'slash-white'))
        else:
            button.setIcon(QtGui.QIcon())
        if self.id == 'task':
            self.gui.filterList()
            self.gui.auto = True
            self.gui.smartSelect()
            self.gui.auto = False
            self.gui.setShotFilter()
        if self.id == 'summary':
            self.gui.showSummary()

    def saveFilter(self):
        name, ok = QtWidgets.QInputDialog.getText(self.root, 'Save Filter', 'Name :')
        if ok:
            with open(self.gui.settingsFile) as fid:
                root = json.load(fid)
            root['filters'][name] = ['!%s' % x if self.filters[x].inverted else x for x in self.filters.keys()]
            lines = json.dumps(root, sort_keys=True, indent=4)
            with open(self.gui.settingsFile, 'w') as fid:
                fid.writelines(lines)
            self.auto = True
            liste = ['', 'me'] + root['filters'].keys() + self.gui.currentProject.availableTasks
            self.root.filtersComboBox.clear()
            self.setItems(liste)
            self.root.filtersComboBox.setCurrentIndex(liste.index(name))
            self.auto = False

            self.root.deleteFilterPushButton.setEnabled(True)
            self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-white'))
            self.root.addFilterPushButton.setEnabled(False)
            self.root.addFilterPushButton.setIcon(icon(self.gui, 'save-grey'))

    def deleteFilter(self):
        name = self.root.filtersComboBox.currentText()
        if len(name):
            with open(self.gui.settingsFile) as fid:
                root = json.load(fid)
            del root['filters'][name]
            lines = json.dumps(root, sort_keys=True, indent=4)
            with open(self.gui.settingsFile, 'w') as fid:
                fid.writelines(lines)
            self.auto = True
            self.setItems(['', 'me'] + root['filters'].keys() + self.gui.currentProject.availableTasks)
            self.root.filtersComboBox.setCurrentIndex(0)
            self.auto = False

            self.root.deleteFilterPushButton.setEnabled(False)
            self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-grey'))
            self.root.addFilterPushButton.setEnabled(True)
            self.root.addFilterPushButton.setIcon(icon(self.gui, 'save-white'))

    def loadFilter(self):
        if self.auto or self.gui.auto:
            return
        name = self.root.filtersComboBox.currentText()
        with open(self.gui.settingsFile) as fid:
            root = json.load(fid)
        root['filters'][''] = []
        root['filters']['me'] = ['user:%s' % os.getenv('USER')]
        for task in self.gui.currentProject.availableTasks:
            root['filters'][task] = ['task:%s' % task]

        for text in list(self.filters.keys()):
            self.filters[text].deleteLater()
            del self.filters[text]
        if len(name):
            for text in root['filters'][name]:
                inverted = text.startswith('!')
                if inverted:
                    text = text[1:]
                self.filters[text] = FilterButton(self, text, inverted)
                self.root.filterLayout.addWidget(self.filters[text])
            self.root.deleteFilterPushButton.setEnabled(True)
            self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-white'))
        else:
            self.root.deleteFilterPushButton.setEnabled(False)
            self.root.deleteFilterPushButton.setIcon(icon(self.gui, 'trash-grey'))
        self.root.addFilterPushButton.setEnabled(False)
        self.root.addFilterPushButton.setIcon(icon(self.gui, 'save-grey'))
        if self.gui.currentProject:
            if self.id == 'task':
                self.gui.filterList()
                self.gui.auto = True
                self.gui.smartSelect()
                self.gui.auto = False
                self.gui.setShotFilter()
            if self.id == 'summary':
                if self.gui.root.summaryWidget.isVisible():
                    self.gui.showSummary()

    def reset(self):
        self.root.filtersComboBox.setCurrentIndex(0)
        for text in list(self.filters.keys()):
            self.filters[text].deleteLater()
            del self.filters[text]

    def setItems(self, liste):
        self.root.filtersComboBox.clear()
        self.root.filtersComboBox.addItems(liste)
        self.gui.root.sortComboBox.setFixedWidth(self.root.filtersComboBox.width() + 56)


class TimeLogUI(QtWidgets.QDialog):
    def __init__(self, gui):
        super(TimeLogUI, self).__init__()
        self.gui = gui
        self.elements = []
        self.lineWidgets = {}
        self.changes = {}
        mainUIPath = os.path.join(self.gui.shaDirectory, 'UI', 'timeLog.ui')
        self.root = QtCompat.loadUi(mainUIPath)
        self.days = [self.root.day1Edit, self.root.day2Edit, self.root.day3Edit, self.root.day4Edit, self.root.day5Edit,
                     self.root.day6Edit, self.root.day7Edit]
        self.setWindowIcon(icon(self.gui, 'shaIcon'))
        self.setStyleSheet(self.gui.style)
        L = QtWidgets.QHBoxLayout()
        L.setContentsMargins(0, 0, 0, 0)
        self.setLayout(L)
        L.addWidget(self.root)
        self.root.addButton.setIcon(icon(self.gui, 'plus-circle'))
        start = datetime.today()
        current_week = int(datetime.date(start).isocalendar()[1])
        self.root.weekComboBox.addItems(["Week %s" % (current_week - x) for x in range(5)])
        self.root.weekComboBox.setStyleSheet('QComboBox::!hover{background: transparent}')
        self.root.addButton.setStyleSheet('QPushButton::!hover {background: transparent}')
        for edit in self.days:
            edit.setStyleSheet('background: %s' % self.gui.selColor['selection'])

        self.projects = {x: aax.Project.Project(x) for x in aax.getProjectNames()}
        self.user = {}
        for project in self.projects.values():
            self.user[project.codeName] = project.getUser(os.getenv('USER'))
            if self.user[project.codeName] is None:
                self.user[project.codeName] = project.addUser(os.getenv('USER'))

        self.chooseElements()
        self.fill()

        self.root.applyButton.clicked.connect(self.apply)
        self.root.cancelButton.clicked.connect(self.close)
        self.root.weekComboBox.currentIndexChanged.connect(self.fill)
        self.root.addButton.clicked.connect(self.add)
        self.show()

    def add(self):
        d = QtWidgets.QDialog(self.root)
        d.setWindowTitle('Add asset')
        d.resize(500, 20)
        v = QtWidgets.QVBoxLayout()
        label = QtWidgets.QLabel('Asset path :')
        edit = QtWidgets.QLineEdit()
        button = QtWidgets.QPushButton('Ok')
        v.addWidget(label)
        v.addWidget(edit)
        v.addWidget(button)
        d.setLayout(v)
        button.clicked.connect(lambda: d.done(1))

        # installe le QCompleter Filter #############################################################
        completer = QtWidgets.QCompleter(edit)
        completer.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        edit.setCompleter(completer)
        # installe le model à partir de la liste fournie datas
        model = QtGui.QStandardItemModel()
        # installe le QSortFilterProxyModel qui s'insère entre le QCompleter et le model
        proxymodel = QtCore.QSortFilterProxyModel(edit)
        proxymodel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        proxymodel.setSourceModel(model)
        completer.setModel(proxymodel)
        # chaque changement de texte déclenchera la filtration du model
        edit.textEdited.connect(proxymodel.setFilterFixedString)

        liste = []
        for project in self.projects.values():
            liste.extend([project.codeName + ':' + x.path for x in project.assets if len(x.tasks)])
        for i, word in enumerate(sorted(liste)):
            item = QtGui.QStandardItem(word)
            model.setItem(i, 0, item)

        out = d.exec_()
        if out:
            text = str(edit.text()).strip()
            project_name = text.split(':')[0]
            if project_name in self.projects:
                project = self.projects[project_name]
                asset_path = text.split(':', 1)[-1]
                asset = project.getAsset(asset_path)
                if asset:
                    new = [project_name, asset.path, asset.tasks[0].name, [x.name for x in asset.tasks]]
                    self.elements.append(new)
                    self.fill()

    def chooseElements(self):
        self.elements = []
        for project in self.projects.values():
            user_logs = []
            for x in self.user[project.codeName].logs.values():
                user_logs.extend([y for y in x if x[y] > 0])
            for asset in project.assets:
                if asset.omit:
                    continue
                for task in asset.tasks:
                    pathA = ':'.join(['path', asset.path, task.name])
                    pathB = ':'.join([project.codeName, asset.path, task.name])
                    if self.user[project.codeName].match(pathA) or pathB in user_logs:
                        if len(task.versions) and task.versions[-1].status in ['WIP', 'In Progress']:
                            self.elements.append([project.codeName, asset.path, task.name, [x.name for x in asset.tasks]])

    def replaceElement(self, text, a):
        for i in range(len(self.elements)):
            if self.elements[i] is a:
                self.elements[i] = [a[0], a[1], text.split(' - ')[-1], a[3]]
                path = ':'.join([self.elements[i][0], self.elements[i][1], self.elements[i][2]])
                self.changes[path] = {}
        self.fill()

    def fill(self):
        clearLayout(self.root.logLayout)

        self.lineWidgets = {}
        week = int(self.root.weekComboBox.currentText()[5:])
        current_week = int(datetime.date(datetime.today()).isocalendar()[1])
        current_year = int(datetime.strftime(datetime.today(), '%Y'))
        year = current_year if week <= current_week else current_year - 1
        for element in self.elements:
            path = ':'.join([element[0], element[1], element[2]])
            self.lineWidgets[path] = []
            v = QtWidgets.QHBoxLayout()
            v.setContentsMargins(0, 0, 0, 0)
            self.root.logLayout.addLayout(v)

            c = QtWidgets.QComboBox()
            c.setMinimumHeight(34)
            c.addItems(['%s - %s' % (element[0] + ' - ' + element[1].split(':', 1)[-1], x) for x in element[3]])
            c.setCurrentIndex(c.findText('%s - %s' % (element[0] + ' - ' + element[1].split(':', 1)[-1], element[2]), flags=QtCore.Qt.MatchFixedString))
            c.currentIndexChanged[str].connect(lambda t, e=element: self.replaceElement(t, e))
            v.addWidget(c)

            for i in range(7):
                day = datetime.strptime('%s-%s-2' % (year, week), "%Y-%W-%w") + timedelta(days=i)
                day = datetime.strftime(day, '%d/%m/%y')

                edit = MyEdit()
                self.lineWidgets[path].append(edit)
                if path in self.changes and day in self.changes[path]:
                    edit.setText(self.changes[path][day])
                elif day in self.user[element[0]].logs and path in self.user[element[0]].logs[day]:
                    edit.setText(str(self.user[element[0]].logs[day][path]))
                v.addWidget(edit)
                edit.setFixedWidth(40)
                edit.setAlignment(QtCore.Qt.AlignHCenter)
                edit.textChanged.connect(lambda t, d=i, dd=day, p=path: self.modify(d, dd, p, t))
        for i in range(7):
            self.modify(i)

    def modify(self, day_num, date=None, path=None, text=None):
        if date is not None:
            if path not in self.changes:
                self.changes[path] = {}
            self.changes[path][date] = text
            try:
                int(text)
                self.lineWidgets[path][day_num].setStyleSheet('')
            except ValueError:
                self.lineWidgets[path][day_num].setStyleSheet('background: #663333')

        result = 0
        for path in self.lineWidgets:
            text = self.lineWidgets[path][day_num].text()
            if len(text):
                try:
                    result += int(text)
                except ValueError:
                    result = '###'
                    self.days[day_num].setStyleSheet('background: #663333')
                    break
        else:
            if result == 8:
                green = self.gui.selColor['selection']
                green = green[:3] + green[5:] + green[3:5]
                self.days[day_num].setStyleSheet('background: %s' % green)
            else:
                self.days[day_num].setStyleSheet('background: %s' % self.gui.selColor['selection'])
        self.days[day_num].setText(str(result))

    def apply(self):
        for project in self.projects:
            dic = {}
            for path in self.changes:
                if path.split(':')[0] == project:
                    for day in self.changes[path]:
                        if day not in dic:
                            dic[day] = {}
                        try:
                            dic[day][path] = int(self.changes[path][day])
                        except ValueError:
                            pass
                self.user[project].addLogs(dic)
        self.close()


class MyTextWidget(QtWidgets.QTextEdit):
    def __init__(self):
        super(MyTextWidget, self).__init__()

    def keyPressEvent(self, event):
        cursor = self.textCursor()

        if event.key() == QtCore.Qt.Key_Tab:
            self.insertPlainText(' ' * 4)
            return False
        if event.key() == QtCore.Qt.Key_Return:
            line = cursor.block().text()
            first = line.lstrip()[0].encode('ascii', 'xmlcharrefreplace')
            if first == '&#8226;':
                self.insertPlainText('\n' + ' ' * (len(line) - len(line.lstrip())))
                self.insertHtml('&bull;')
                self.insertPlainText(' ')
                return False
        if event.key() == ord('-'):
            i = cursor.positionInBlock()
            if len(cursor.block().text()[:i].strip()) == 0:
                if len(cursor.block().text()[:i]) == 0:
                    self.insertPlainText(' ' * 4)
                self.insertHtml('&bull;')
                self.insertPlainText(' ')
                return False
        super(MyTextWidget, self).keyPressEvent(event)


class CommentUI(QtWidgets.QDialog):
    def __init__(self, gui, comment, edit, color, attachHeight, layout, scroll, master, first=False, newL=True):
        super(CommentUI, self).__init__()
        self.gui = gui
        self.color = color
        self.comment = comment
        self.edit = edit
        self.first = first
        self.newL = newL
        self.master = master
        self.attachHeight = attachHeight
        self.layout = layout
        self.scroll = scroll
        mainUIPath = os.path.join(self.gui.shaDirectory, 'UI', 'comment.ui')
        self.root = QtCompat.loadUi(mainUIPath)
        self.setWindowIcon(icon(self.gui, 'shaIcon'))
        self.root.commentEdit = MyTextWidget()
        self.root.commentLayout.addWidget(self.root.commentEdit)

        self.root.linkButton.setIcon(icon(self.gui, 'link'))
        self.root.linkWidget.setStyleSheet('background: %s' % self.gui.selColor['selection'])
        self.root.linkLineEdit.setText(comment.link)
        
        L = QtWidgets.QHBoxLayout()
        L.setContentsMargins(0, 0, 0, 0)
        self.setLayout(L)
        L.addWidget(self.root)

        self.attachments = list(comment.attachments)

        self.root.boldButton.clicked.connect(self.bold)
        self.root.italicButton.clicked.connect(self.italic)
        self.root.underlineButton.clicked.connect(self.underline)
        self.root.colorButton.clicked.connect(self.pickColor)

        self.root.levelComboBox.currentIndexChanged[str].connect(self.changeLevel)

        self.root.commentEdit.setHtml(comment.text)
        self.fillAttachments()

        self.root.cancelPushButton.clicked.connect(self.cancel)
        self.root.okPushButton.clicked.connect(self.apply)
        self.root.addButton.clicked.connect(self.addAttachment)
        self.setAcceptDrops(True)

        index = self.root.levelComboBox.findText(comment.flag, flags=QtCore.Qt.MatchFixedString)
        if index > -1:
            self.root.levelComboBox.setCurrentIndex(index)

        self.setStyleSheet(self.gui.style)
        self.root.colorButton.setIcon(icon(self.gui, 'color'))
        self.root.italicButton.setIcon(icon(self.gui, 'format_italic'))
        self.root.boldButton.setIcon(icon(self.gui, 'format_bold'))
        self.root.underlineButton.setIcon(icon(self.gui, 'format_underline'))
        self.root.addButton.setIcon(icon(self.gui, 'paperclip'))
        self.root.colorButton.setIconSize(QtCore.QSize(24, 24))
        self.root.italicButton.setIconSize(QtCore.QSize(24, 24))
        self.root.boldButton.setIconSize(QtCore.QSize(24, 24))
        self.root.underlineButton.setIconSize(QtCore.QSize(24, 24))
        self.root.addButton.setIconSize(QtCore.QSize(24, 24))
        self.root.colorButton.setStyleSheet('QPushButton::!hover{background: transparent}')
        self.root.italicButton.setStyleSheet('QPushButton::!hover{background: transparent}')
        self.root.boldButton.setStyleSheet('QPushButton::!hover{background: transparent}')
        self.root.underlineButton.setStyleSheet('QPushButton::!hover{background: transparent}')
        self.root.addButton.setStyleSheet('QPushButton::!hover{background: transparent}')

        self.resize(800, 400)
        self.show()
        self.root.commentEdit.setFocus()

    def changeLevel(self, s):
        if s == 'Artist':
            self.root.levelComboBox.setStyleSheet('')
        else:
            self.root.levelComboBox.setStyleSheet('''QComboBox{background: %s}
                QComboBox QAbstractItemView {background-color: %s;}''' % (self.gui.colorDic[s],
                                                                          self.gui.selColor['dark']))

    def cancel(self):
        if self.first:
            self.gui.deleteComment(self.comment, self.newL, False, self.master)
        self.close()

    def pickColor(self):
        fmt = QtGui.QTextCharFormat()
        cursor = self.root.commentEdit.textCursor()
        color = cursor.charFormat().foreground().color()
        color = QtWidgets.QColorDialog.getColor(color, self.root)
        fmt.setForeground(color)
        cursor.mergeCharFormat(fmt)
        self.root.commentEdit.mergeCurrentCharFormat(fmt)

    def bold(self):
        fmt = QtGui.QTextCharFormat()
        cursor = self.root.commentEdit.textCursor()
        bold = not cursor.charFormat().font().bold()
        fmt.setFontWeight(QtGui.QFont.Normal if bold else QtGui.QFont.Light)
        cursor.mergeCharFormat(fmt)
        self.root.commentEdit.mergeCurrentCharFormat(fmt)

    def italic(self):
        fmt = QtGui.QTextCharFormat()
        cursor = self.root.commentEdit.textCursor()
        italic = not cursor.charFormat().font().italic()
        fmt.setFontItalic(italic)
        cursor.mergeCharFormat(fmt)
        self.root.commentEdit.mergeCurrentCharFormat(fmt)

    def underline(self):
        fmt = QtGui.QTextCharFormat()
        cursor = self.root.commentEdit.textCursor()
        underline = not cursor.charFormat().font().underline()
        fmt.setFontUnderline(underline)
        cursor.mergeCharFormat(fmt)
        self.root.commentEdit.mergeCurrentCharFormat(fmt)

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
            for url in event.mimeData().urls():
                self.attachments.append(url.toLocalFile())
            self.fillAttachments()
        else:
            event.ignore()

    def dragEnterEvent(self, event):
        event.accept()

    def apply(self):
        text = self.root.commentEdit.toHtml()
        self.comment.setText(text)
        self.comment.setAttachments(self.attachments)
        self.comment.setFlag(self.root.levelComboBox.currentText())
        self.edit.setHtml(text)
        self.comment.setLink(self.root.linkLineEdit.text())
        fm = QtGui.QFontMetrics(QtGui.QFont('Whitney', pointSize=12, weight=12))
        self.edit.setFixedHeight(fm.lineSpacing() * (self.edit.toPlainText().count('\n') + 1) + 12)

        if self.comment.flag not in self.gui.colorDic.keys():
            self.comment.setFlag('Artist')
        if self.comment.flag == 'Artist':
            self.edit.setStyleSheet('border-radius:0; background-color: %s' % self.color)
        else:
            self.edit.setStyleSheet('border-radius:0; background-color: %s' % self.gui.colorDic[self.comment.flag])

        self.gui.fillAttachments(self.layout, self.comment, self.attachHeight, self.scroll)
        self.close()

    def fillAttachments(self):
        clearLayout(self.root.attachmentLayout)
        for attach in self.attachments:
            button = QtWidgets.QPushButton(os.path.basename(attach))
            button.clicked.connect(lambda a=attach: self.removeAttachment(a))
            button.setStyleSheet('text-align: left')
            self.root.attachmentLayout.addWidget(button)

    def removeAttachment(self, attach):
        self.attachments.remove(attach)
        self.fillAttachments()

    def addAttachment(self):
        out = QtWidgets.QFileDialog.getOpenFileName(self.root, "Choose an image")[0]
        if len(out):
            self.attachments.append(out)
            self.fillAttachments()


class UserUI(object):
    def __init__(self, gui):
        self.gui = gui
        self.users = []
        for user in self.gui.currentProject.users:
            self.users.append(aax.User(uuid=user.uuid, project=user.project, name=user.name,
                                       assign=list(user.assign)))
        mainUIPath = os.path.join(self.gui.shaDirectory, 'UI', 'user.ui')
        self.root = QtCompat.loadUi(mainUIPath)
        self.root.setStyleSheet(self.gui.style)
        self.root.userListWidget.addItems(sorted([x.name for x in self.gui.currentProject.users]))
        self.root.addUserButton.setIcon(icon(self.gui, 'user'))
        self.root.delUserButton.setIcon(icon(self.gui, 'trash-white'))
        self.root.addUserButton.setIconSize(QtCore.QSize(16, 16))
        self.root.delUserButton.setIconSize(QtCore.QSize(16, 16))
        self.root.userListWidget.itemSelectionChanged.connect(self.showUser)
        self.root.assignWidget.setStyleSheet('QWidget#assignWidget{background: %s}' % self.gui.selColor['dark'])

        self.root.addUserButton.clicked.connect(self.addUser)
        self.root.delUserButton.clicked.connect(self.delUser)
        self.root.addButton.clicked.connect(self.addAssign)
        self.root.cancelPushButton.clicked.connect(self.root.close)
        self.root.applyPushButton.clicked.connect(self.apply)
        self.root.setWindowTitle('Users assignments')
        self.root.show()

    def getUser(self, name):
        for user in self.users:
            if user.name == name:
                return user
        return None

    def addAssign(self):
        sel = self.root.userListWidget.selectedItems()
        if len(sel):
            sel = sel[0].text()
        else:
            return
        user = self.getUser(sel)
        user.addAssign('path:')
        self.showUser()
        self.root.assignWidget.layout().itemAt(0).itemAt(0).widget().setFocus()

    def addUser(self):
        name, ok = QtWidgets.QInputDialog.getText(self.root, 'New User', 'Name :', text='')
        if ok:
            if name not in [x.name for x in self.users]:
                self.users.append(aax.User(name=name, project=self.gui.currentProject))
                self.root.userListWidget.clear()
                self.root.userListWidget.addItems(sorted([x.name for x in self.users]))

    def delUser(self):
        sel = self.root.userListWidget.selectedItems()
        if len(sel):
            sel = sel[0].text()
        else:
            return
        result = QtWidgets.QMessageBox.question(self.root, 'Confirm',
                                                'Delete user %s ?' % sel,
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                QtWidgets.QMessageBox.No)
        if result == QtWidgets.QMessageBox.Yes:
            self.users.remove(self.getUser(sel))
            self.root.userListWidget.clear()
            self.root.userListWidget.addItems(sorted([x.name for x in self.users]))

    def showUser(self):
        clearLayout(self.root.assignWidget.layout())
        sel = self.root.userListWidget.selectedItems()
        if len(sel):
            sel = sel[0].text()
        else:
            return
        user = self.getUser(sel)
        for assign in sorted(user.assign):
            h = QtWidgets.QHBoxLayout()
            h.setSpacing(0)
            line = QtWidgets.QLineEdit()
            line.setText(assign)
            line.editingFinished.connect(lambda u=user, edit=line, old=assign: self.assignChanged(u, edit, old))
            d = QtWidgets.QPushButton()
            d.setIcon(icon(self.gui, 'trash-white'))
            d.setIconSize(QtCore.QSize(16, 16))
            d.setFlat(True)
            d.setStyleSheet('QPushButton::!hover{background: transparent}')
            d.clicked.connect(lambda u=user, edit=line, layout=h: self.removeAssign(u, edit, layout))
            h.addWidget(line)
            h.addWidget(d)
            self.root.assignWidget.layout().addLayout(h)
        self.root.assignWidget.layout().addStretch()

    def assignChanged(self, user, edit, old):
        assign = [x if x != old else edit.text() for x in user.assign]
        user.setAssigns(assign)
        edit.editingFinished.connect(lambda u=user, e=edit, o=edit.text(): self.assignChanged(u, e, o))

    def removeAssign(self, user, edit, layout):
        user.removeAssign(edit.text())
        clearLayout(layout)
        layout.deleteLater()

    def apply(self):
        for user in list(self.gui.currentProject.users):
            user.delete()
        self.gui.currentProject.users = self.users
        for user in self.gui.currentProject.users:
            user.create()
        self.root.close()


class StatView(QtWidgets.QGraphicsView):
    def __init__(self, parent):
        super(StatView, self).__init__()
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setMouseTracking(1)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)
        self.parent = parent

    def mouseMoveEvent(self, event):
        items = self.items(event.x(), event.y())
        for item in self.items():
            item.unselect()
        for item in items:
            pos = self.mapToScene(event.x(), event.y())
            pos = item.mapFromScene(pos.x(), pos.y())
            item.mouseOver(pos.x(), pos.y())

    def mousePressEvent(self, event):
        items = self.items()
        items = [x for x in items if x.selected]
        if len(items):
            self.parent.selectStat(items[0].title, items[0].selected)

    def resizeEvent(self, event):
        h = max(self.geometry().height(), self.parent.statCount[1])
        self.parent.statScene.setSceneRect(0, 0, 300, h)


class Pie(QtWidgets.QGraphicsItem):
    def __init__(self, gui, tasks, title, radius):
        super(Pie, self).__init__()
        self.gui = gui
        self.radius = radius
        self.rect = QtCore.QRectF(-self.radius, -self.radius, 450, self.radius * 2 + 20)
        self.dico = {}
        self.total = 0
        self.title = title
        self.seed = random.random()
        self.max = 0
        for task in tasks:
            if len(task.versions):
                if task.versions[-1].status not in self.dico:
                    self.dico[task.versions[-1].status] = 0
                self.dico[task.versions[-1].status] += 1
            else:
                if 'Not Started' not in self.dico:
                    self.dico['Not Started'] = 0
                self.dico['Not Started'] += 1
            self.total += 1
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.grow)
        self.timer.start(30)
        self.mousePos = None
        self.selected = None

    def mouseOver(self, x, y):
        self.mousePos = (x, y)
        self.update()

    def unselect(self):
        self.mousePos = (-1000, 0)
        self.update()

    def grow(self):
        self.max = self.max * .8 + 360.0 * .2
        if self.max > 359:
            self.max = 360
            self.timer.stop()
        self.update()

    def boundingRect(self):
        return self.rect

    def shape(self):
        path = QtGui.QPainterPath()
        path.addRect(self.rect)
        return path

    def paint(self, painter, option, widget):
        pen = QtGui.QPen()
        pen.setWidth(2)
        pen.setColor(QtGui.QColor(200, 200, 200, 0))
        painter.setPen(pen)
        start = 0.0
        y = -self.radius + 30
        self.selected = None
        f = QtGui.QFont()
        f.setPixelSize(20)
        painter.setFont(f)
        for key in self.dico.keys():
            if start < self.max:
                points = [QtCore.QPoint(0, 5)]
                end = start + 360.0 * self.dico[key] / self.total
                i = start
                while i < end + 1 and i < self.max:
                    points.append(QtCore.QPoint(math.cos(i * 3.141592 / 180.0) * self.radius,
                                                math.sin(i * 3.141592 / 180.0) * self.radius + 5))
                    i += 1

                random.seed(key)
                hue = QtGui.QColor(self.gui.selColor['selection']).hslHue()
                color = QtGui.QColor.fromHsl(hue, 100, random.random() * 70 + 30)
                sel = self.gui.selColor['pieSelection']
                text = "%s (%s)" % (key, self.dico[key])

                if self.mousePos:
                    w = painter.fontMetrics().width(text) + 30
                    h = painter.fontMetrics().height()
                    if self.radius + 20 < self.mousePos[0] < self.radius + 20 + w:
                        if y + h > self.mousePos[1] > y:
                            color = QtGui.QColor(sel)
                            self.selected = key
                    else:
                        dist = math.sqrt(self.mousePos[0]*self.mousePos[0] + self.mousePos[1]*self.mousePos[1])
                        if dist < self.radius:
                            px = self.mousePos[0] if self.mousePos[0] != 0 else 0.0001
                            angle = math.atan(self.mousePos[1] / px) * 180 / 3.141592
                            if self.mousePos[0] > 0:
                                if self.mousePos[1] < 0:
                                    angle = 360 + angle
                            else:
                                angle = 180 + angle
                            if start < angle < end:
                                color = QtGui.QColor(sel)
                                self.selected = key

                painter.setBrush(color)
                pen.setColor(color)
                painter.setPen(pen)
                if len(self.dico.keys()) == 1:
                    painter.drawEllipse(-self.radius, -self.radius, self.radius * 2, self.radius * 2)
                else:
                    painter.drawPolygon(points)

                painter.drawRect(self.radius + 20, y, 15, 15)

                painter.setPen(pen)
                if len(self.dico.keys()) != 1:
                    painter.drawPolyline(points[1:])

                if self.selected == key:
                    color = QtGui.QColor(sel)
                else:
                    color = QtGui.QColor(self.gui.selColor['grey'])
                color.setAlpha(255)
                pen.setColor(color)
                painter.setPen(pen)
                painter.drawText(self.radius + 40, y + 13, text)

                y = y + 25
                start = end

        f = QtGui.QFont()
        f.setPixelSize(20)
        painter.setFont(f)
        pen.setColor(QtGui.QColor(200, 200, 200, 255))
        painter.setPen(pen)
        painter.drawText(self.radius + 60, -self.radius, "%s (%s)" % (self.title, self.total))
        pen.setColor(QtGui.QColor(100, 100, 100, 255))
        painter.setPen(pen)
        w = painter.fontMetrics().width("%s (%s)" % (self.title, self.total))
        painter.drawLine(-self.radius - 20, -self.radius - 7, self.radius + 50, -self.radius - 7)
        painter.drawLine(self.radius + 60 + 20 + w, -self.radius - 7, 550, -self.radius - 7)


def main():
    app = QtWidgets.QApplication(sys.argv)
    a = ShamanGUI(app)
    sys.exit(app.exec_())


try:
    set_procname('shaman')
except:
    pass
main()
